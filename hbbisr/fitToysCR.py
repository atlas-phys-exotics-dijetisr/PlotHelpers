from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import utiltools
from prot import fittools

import timeit

from hbbisr import funcfactory
from hbbisr import hbbfittools
from hbbisrnote import constants

import ROOT

import numpy as np

import sys
import array

def main(seed=0,selectioncode='cr',fname='exppol4',mrange=(70,230),lumi=80.6,peaks=['v','ttbar'],peaklumi=None):
    ROOT.gRandom.SetSeed(seed)

    if peaklumi==None: 
        peaklumi=lumi

    #
    # Create histograms
    fh_sig  =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-sig.root')
    fh_v    =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-v.root')
    fh_qcd  =filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-qcd.root')
    fh_ttbar=filetools.open('/global/projecta/projectdirs/atlas/kkrizka/batch/hist_20180508/OUT_fatjet_mc/hist-ttbar.root')

    selectionqcd=''
    selectionsig=''
    if selectioncode=='cr':
        selectionqcd=constants.selectionCR.replace('_hm40','')
        selectionsig=constants.selectionCR.replace('_hm40','')
        title='CR - Pythia 8 QCD'
    elif selectioncode=='vr':
        selectionqcd=constants.selectionVR.replace('_hm40','')
        selectionsig=constants.selectionVR.replace('_hm40','')
        title='VR - Pythia 8 QCD'
    elif selectioncode=='sr':
        selectionqcd=constants.selectionSR.replace('_hm40','')
        selectionsig=constants.selectionSR.replace('_hm40','')
        title='SR - Pythia 8 QCD'
    elif selectioncode=='crsr':
        selectionqcd=constants.selectionCR.replace('_hm40','')
        selectionsig=constants.selectionSR.replace('_hm40','')
        title='SR - Pythia 8 QCD\nCR for QCD model'
    else:
        print('Unknown selection code {}'.format(selectioncode))
        return

    h_sig  =fh_sig  .Get('{}/Hcand_m_s'.format(selectionsig))
    h_v    =fh_v    .Get('{}/Hcand_m_s'.format(selectionsig))
    h_qcd  =fh_qcd  .Get('{}/Hcand_m_s'.format(selectionqcd))
    h_ttbar=fh_ttbar.Get('{}/Hcand_m_s'.format(selectionsig))

    h_sig  .Scale(peaklumi*1e3)
    h_v    .Scale(peaklumi*1e3)
    h_qcd  .Scale(lumi*1e3)
    h_ttbar.Scale(peaklumi*1e3)

    peakmap={}
    peakmap['sig'  ]=h_sig
    peakmap['v'    ]=h_v
    peakmap['ttbar']=h_ttbar

    f_qcd=funcfactory.funcs['f'+fname]
    r=fittools.fit(h_qcd,'f'+fname,fitrange=mrange,fitopt='i')
    bestfit=list(r.Parameters())

    fitnames=['qcd']+peaks
    fitwiths=[f_qcd]+[peakmap[peak] for peak in peaks]
    fitinits=[f_qcd.GetParameter(i) for i in range(f_qcd.GetNpar())]+[1.]*len(peaks)
    fitparam=[]
    idx=0
    for fitwith in fitwiths:
        if type(fitwith)==ROOT.TF1:
            for i in range(fitwith.GetNpar()):
                fitparam.append('{}inj'.format(fitwith.GetParName(i)))
                fitparam.append('{}fit'.format(fitwith.GetParName(i)))
                fitparam.append('{}fitunc'.format(fitwith.GetParName(i)))
        else:
            fitparam.append('mu{}inj'.format(fitnames[idx]))
            fitparam.append('mu{}fit'.format(fitnames[idx]))
            fitparam.append('mu{}fitunc'.format(fitnames[idx]))
        idx+=1
    print(fitparam)
    fh=open('fitdata/test_lumi{lumi}_{selection}_{fname}_{peakstr}_{mrange0}to{mrange1}_{seed:05d}.csv'.format(lumi=lumi,selection=selectioncode,fname=fname,peakstr='_'.join(peaks),mrange0=mrange[0],mrange1=mrange[1],seed=seed),'w')
    fh.write('status {}\n'.format(' '.join(fitparam)))

    h_data=h_qcd.Clone()
    for i in range(100):
        # Generate pseudo-data        
        h_data.Reset()

        f_qcd.SetParameters(*bestfit)
        for fitwith in fitwiths:
            if type(fitwith)==ROOT.TF1:
                h_data.FillRandom(fitwith.GetName(),ROOT.gRandom.Poisson(fitwith.Integral(h_data.GetBinLowEdge(1),h_data.GetBinLowEdge(h_data.GetNbinsX()+1))/h_data.GetBinWidth(1)))
            else:
                h_data.FillRandom(fitwith,ROOT.gRandom.Poisson(fitwith.Integral()))

        # Fit the pseudo-data
        start_time = timeit.default_timer()
        result=hbbfittools.fitDataMC(fitwiths,h_data,mrange=mrange,guess=fitinits)
        elapsed = timeit.default_timer() - start_time
        print(elapsed)

        tosave=[]
        tosave.append(result.Status())

        argidx=0
        for argidx in range(len(fitinits)):
            tosave.append(fitinits[argidx])
            tosave.append(result.Parameter(argidx))
            tosave.append(result.ParError(argidx))
            argidx+=1

        fh.write(' '.join(['{}'.format(x) for x in tosave])+'\n')
        fh.flush()

        #
        # Draw example
        if i!=0 or seed!=0: continue

        h_qcd=h_data.Clone()
        utiltools.store.append(h_qcd)
        h_qcd.Reset()
        for binidx in range(h_qcd.FindBin(mrange[0]),h_qcd.FindBin(mrange[1])):
            val=f_qcd.Integral(h_qcd.GetBinLowEdge(binidx),h_qcd.GetBinLowEdge(binidx+1))/h_qcd.GetBinWidth(binidx)
            h_qcd.SetBinContent(binidx,val)
    
        plottools.plots([(h_qcd,{'title':'QCD',
                                 'fillcolor':ROOT.UCYellow,
                                 'opt':'hist'}),
                         (h_data,{'title':'Data'})],
                        hsopt='nostack',
                        text='{title}\nToys'.format(title=title),lumi=constants.lumi,sim=True,textpos=(0.5,0.7),
                        legend=(0.5,0.93),legendncol=2,
                        ytitle='Events',
                        xrange=mrange,
                        ratiomode='resid')
        canvastools.save('{selectioncode}/toyfit_{fname}_{mrangemin}to{mrangemax}.pdf'.format(selectioncode=selectioncode,fname=fname,mrangemin=mrange[0],mrangemax=mrange[1]))

    fh.close()
