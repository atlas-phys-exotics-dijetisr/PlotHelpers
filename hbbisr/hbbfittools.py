import array

import numpy as np

import ROOT

class FitFunction(ROOT.TPyMultiGenFunction):
    def __init__(self,mchists,datahist,mrange):
        ROOT.TPyMultiGenFunction.__init__(self, self)

        self.datahist=datahist

        self.binmin=datahist.FindBin(mrange[0])
        self.binmax=datahist.FindBin(mrange[1])

        # Pythonize the histogram contents
        data=[]
        dataerr2=[]
        for binidx in range(self.binmin,self.binmax):
            data.append(datahist.GetBinContent(binidx))
            dataerr2.append(datahist.GetBinError(binidx)*datahist.GetBinError(binidx))
        self.data=np.array(data)
        self.dataerr2=np.array(dataerr2)

        self.mcs=[]
        for mchist in mchists:
            if type(mchist)==tuple:
                mc=[]
                for binidx in range(self.binmin,self.binmax):
                    tot=0
                    for mcsubhist in mchist:
                        tot+=mcsubhist.GetBinContent(binidx)
                    mc.append(tot)
                self.mcs.append(np.array(mc))
            elif mchist.InheritsFrom(ROOT.TH1.Class()):
                mc=[]
                for binidx in range(self.binmin,self.binmax):
                    mc.append(mchist.GetBinContent(binidx))
                self.mcs.append(np.array(mc))                    
            else:
                self.mcs.append(mchist)

    def NDim(self):
        npar=0;
        for mc in self.mcs:
            if type(mc)==np.ndarray:
                npar+=1 # just normalization
            elif mc.InheritsFrom(ROOT.TF1.Class()):
                npar+=mc.GetNpar()
        return npar

    def DoEval(self, args):
        # Set any functions
        argidx=0
        for mc in self.mcs:
            if type(mc)==np.ndarray:
                argidx+=1
            else:
                for i in range(mc.GetNpar()):
                    mc.SetParameter(i,args[argidx])
                    argidx+=1

        # Make MC hist
        summc=np.array([0.]*self.data.shape[0])
        argidx=0
        for mc in self.mcs:
            if type(mc)==np.ndarray:
                summc+=args[argidx]*mc
                argidx+=1
            else:
                for binidx in range(self.binmin,self.binmax):
                    #summc[binidx-self.binmin]+=mc.Eval(self.datahist.GetBinCenter(binidx))
                    summc[binidx-self.binmin]+=mc.Integral(self.datahist.GetBinLowEdge(binidx),self.datahist.GetBinLowEdge(binidx+1))/self.datahist.GetBinWidth(binidx)
                argidx+=mc.GetNpar()
        return (np.square(summc-self.data)/self.dataerr2).sum()

def fitDataMC(hs_ms,h_data,fix=None,guess=None,mrange=(60,200)):
    #
    # Create a fitter
    thisFitter = ROOT.Fit.Fitter()
    thisFitter.Config().MinimizerOptions().SetMinimizerType("Minuit2")
    thisFitter.Config().MinimizerOptions().SetMinimizerAlgorithm("Minimize")
    thisFitter.Config().MinimizerOptions().SetMaxIterations(1000000)
    thisFitter.Config().MinimizerOptions().SetMaxFunctionCalls(100000)
    thisFitter.Config().MinimizerOptions().SetStrategy(1)

    #
    # Create function
    thisFunctor=FitFunction(hs_ms,h_data,mrange)

    # Setup parameters
    if guess==None: guess=[1.]*thisFunctor.NDim()
    
    aParams = array.array( 'd', guess) #[1.]*thisFunctor.NDim())
    aSteps  = array.array( 'd', [0.01]*thisFunctor.NDim())
    thisFitter.Config().SetParamsSettings(thisFunctor.NDim(), aParams, aSteps)

    if fix!=None:
        for parIdx in fix:
            thisFitter.Config().ParamsSettings()[parIdx].Fix()
    
    #
    # Run the fit
    thisFitter.FitFCN(thisFunctor)
    result=ROOT.TFitResult(thisFitter.Result())
    result.Print()
    return result

