from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import utiltools

import fitdatamctools

from hbbisrnote import constants

import ROOT

import sys
import array

ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize")
ROOT.Math.MinimizerOptions.SetDefaultMaxIterations(1000000)
ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
ROOT.Math.MinimizerOptions.SetDefaultTolerance(0.1)

class FitFunction(ROOT.TPyMultiGenFunction):
    def __init__(self,mchists,datahist,lumi):
        ROOT.TPyMultiGenFunction.__init__(self, self)

        self.lumi=lumi
        
        self.mchists=mchists
        self.datahist=datahist

        self.binmin=datahist.FindBin(60)
        self.binmax=datahist.FindBin(200)

    def NDim(self):
        npar=0;
        for mchist in self.mchists:
            if type(mchist)==tuple or mchist.InheritsFrom(ROOT.TH1.Class()):
                npar+=1 # just normalization
            elif mchist.InheritsFrom(ROOT.TF1.Class()):
                npar+=mchist.GetNpar()
        return npar

    def DoEval(self, args):
        # Set any functions
        argidx=0
        for mchist in self.mchists:
            if type(mchist)==tuple or mchist.InheritsFrom(ROOT.TH1.Class()):
                argidx+=1
            else:
                for i in range(mchist.GetNpar()):
                    mchist.SetParameter(i,args[argidx])
                    argidx+=1
        # Calculate chi2
        totalchi2=0.
        for binidx in range(self.binmin,self.binmax):
            data=self.datahist.GetBinContent(binidx)
            err=self.datahist.GetBinError(binidx)
            binx=self.datahist.GetBinCenter(binidx)
            mc=0
            argidx=0
            for mchist in self.mchists:
                if type(mchist)==tuple:
                    for mcsubhist in mchist:
                        mc+=args[argidx]*mcsubhist.GetBinContent(mcsubhist.FindBin(binx))*self.lumi*1e3
                    argidx+=1
                elif mchist.InheritsFrom(ROOT.TH1.Class()):
                    mc+=args[argidx]*mchist.GetBinContent(mchist.FindBin(binx))*self.lumi*1e3
                    argidx+=1
                else:
                    argidx+=mchist.GetNpar()
                    mc+=mchist.Integral(self.datahist.GetBinLowEdge(binidx),self.datahist.GetBinLowEdge(binidx+1))#/self.datahist.GetBinWidth(binidx)
                    #mc+=mchist.Integral(self.datahist.GetBinLowEdge(binidx),self.datahist.GetBinLowEdge(binidx+1))
                    #mc+=mchist.Eval(self.datahist.GetBinCenter(binidx))
            chi2=(data-mc)**2/err**2
            totalchi2+=chi2
        return totalchi2

def fitDataMC(hs_ms,h_data,lumi,fix=None):
    thisFunctor=FitFunction(hs_ms,h_data,lumi=lumi)

    #
    # Create a fitter
    thisFitter = ROOT.Fit.Fitter()
    thisFitter.Config().MinimizerOptions().SetMinimizerType("Minuit2")
    thisFitter.Config().MinimizerOptions().SetMinimizerAlgorithm("Minimize")
    thisFitter.Config().MinimizerOptions().SetMaxIterations(1000000)
    thisFitter.Config().MinimizerOptions().SetMaxFunctionCalls(100000)
    thisFitter.Config().MinimizerOptions().SetStrategy(1)

    # Setup parameters
    a=[12023.6,-229.949,11426.9,-338182]    
    
    aParams = array.array( 'd', [1.]*thisFunctor.NDim())
    aSteps  = array.array( 'd', [0.01]*thisFunctor.NDim())
    thisFitter.Config().SetParamsSettings(thisFunctor.NDim(), aParams, aSteps)

    if fix!=None:
        for parIdx in fix:
            thisFitter.Config().ParamsSettings()[parIdx].Fix()
    
    #
    # Run the fit
    thisFitter.FitFCN(thisFunctor)
    result=ROOT.TFitResult(thisFitter.Result())
    result.Print()
    return result


def main():
    #
    # Create histograms
    fh_w    =filetools.open('hist_20180329/OUT_fatjet_mc/hist-w.root')
    fh_z    =filetools.open('hist_20180329/OUT_fatjet_mc/hist-z.root')
    fh_qcd  =filetools.open('hist_20180329/OUT_fatjet_mc/hist-qcd.root')
    fh_ttbar=filetools.open('hist_20180329/OUT_fatjet_mc/hist-ttbar.root')
    fh_data =filetools.open('generated_0tag_QCD_exp_3param_2tag_Z_template_toy.root')

    selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_d2sort0_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77'
    h_w    =fh_w    .Get('{}/Hcand_m'.format(selection))
    h_z    =fh_z    .Get('{}/Hcand_m'.format(selection))
    h_qcd  =fh_qcd  .Get('{}/Hcand_m'.format(selection))
    h_ttbar=fh_ttbar.Get('{}/Hcand_m'.format(selection))
    #h_data =fh_data .Get('gen_0tag_QCD_2tag_Z_toy19985__x')

    f_qcd=ROOT.TF1('fqcd','[0]*exp([1]*(x/13000)+[2]*(x/13000)**2+[3]*(x/13000)**3)',0,500)

    h_pull=ROOT.TH1F('pulls','',100,-2,2)
    i=0
    for key in fh_data.GetListOfKeys():
        h_data=key.ReadObj()

        result=fitDataMC([f_qcd,h_z],h_data,lumi=80.3)
        pull=(result.Parameter(4)-1) #/result.ParError(4)
        h_pull.Fill(pull)
        i+=1
        if i==10: break

    plottools.plot(h_pull)
        
#     h_qcd=h_data.Clone()
#     utiltools.store.append(h_qcd)
#     h_qcd.Reset()
#     for binidx in range(h_qcd.FindBin(60),h_qcd.FindBin(200)):
#         val=f_qcd.Integral(h_qcd.GetBinLowEdge(binidx),h_qcd.GetBinLowEdge(binidx+1))/h_qcd.GetBinWidth(binidx)
#         h_qcd.SetBinContent(binidx,val)
    
#     plottools.plots([(h_qcd,{'title':'QCD',
#                              'fillcolor':ROOT.UCYellow,
#                              'opt':'hist'}),
#                      (h_data,{'title':'Data'})],
#                     hsopt='nostack',
#                     text='CRqcd - Scaled Pythia8 QCD',lumi=1.236,sim=True,textpos=(0.5,0.7),
#                     legend=(0.5,0.93),legendncol=2,
#                     ytitle='Events',
#                         xrange=(60,200),
#                         ratiomode='resid')
# #                        ratiorange=(-200,200),ratiotitle='Data-Fit',ratiomode='resid')
#     canvastools.save('{}/fitCR2015.pdf'.format(selection))
