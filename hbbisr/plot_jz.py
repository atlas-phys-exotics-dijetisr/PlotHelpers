import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import style

import selectiontools

from hbbisrnote import constants

from math import *

def plot_jz(hname, jzs):
    hists=[]
    ROOT.gStyle.SetPalette(ROOT.kPastel)
    plottools.plotsf(jzs,'M100to150/{}'.format(hname),
                     hsopt='pfc',opt='hist',
                     ytitle='Events',
                     text='Pythia8 JZ*W Slices',textpos=(0.55,0.8),lumi=constants.lumi,
                     legend=(0.55,0.75))

def runAllJZPlot(*jzs,**kwargs):
    for key in jzs[0][0].GetListOfKeys():
        if key.GetName()!='Hcand_pt_m': continue
        if key.ReadObj().InheritsFrom(ROOT.TH1F.Class()):
            plot_jz(key.GetName(),jzs)
    
def main(**kwargs):
    canvastools.savesetup('jz',**kwargs)

    jzws=[]
    for jz in range(13):
        filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.{dsid}.Pythia8EvtGen_A14NNPDF23LO_jetjet_JZ{jz}W.fatjet.20180903-01_tree.root.root'.format(dsid=361020+jz,jz=jz),'jz{}w'.format(jz))
        jzws.append(('jz{}w'.format(jz),{'title':'JZ{}W'.format(jz)}))

    selectiontools.loop(runAllJZPlot, *jzws, selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt250_fj1pt*_tjet2_*sort0_vetottbarCR_hpt*_hm40_nbtag2in2ext_MV2c10_FixedCutBEff_77', **kwargs)

    canvastools.savereset()
