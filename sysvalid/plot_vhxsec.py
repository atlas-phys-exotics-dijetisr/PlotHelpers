from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools

import ROOT

def make_xsecgraph(dirpath):
    h_pt=utiltools.Get(dirpath+'/Zprime_pt')

    g_xsec=ROOT.TGraph()
    gidx=0
    for binidx in range(1,h_pt.GetNbinsX()+1):
        pt=h_pt.GetBinLowEdge(binidx)
        xsec=h_pt.Integral(binidx,h_pt.GetNbinsX()+1)
        g_xsec.SetPoint(gidx,pt,xsec)
        gidx+=1
        print(pt,xsec)
    return g_xsec

def main():
    filetools.filemap('ggZH.root','vh')
    title='ggZH'
    yrange=(1e-8,10)
    # VH
    murrange=(0.9,1.1)
    pdfrrange=(0.9,1.1)
    # ggZH
    murrange=(0.5,1.5)
    pdfrrange=(0.8,1.2)
    
    
    #
    # Nominal
    g_xsec=make_xsecgraph('vh:/nominal')

    #
    # Scale uncertainty
    g_xsec_muR05muF05=make_xsecgraph('vh:/muR05muF05')
    g_xsec_muR05muF10=make_xsecgraph('vh:/muR05muF10')
    g_xsec_muR10muF05=make_xsecgraph('vh:/muR10muF05')
    g_xsec_muR10muF10=make_xsecgraph('vh:/nominal')
    g_xsec_muR10muF20=make_xsecgraph('vh:/muR10muF20')
    g_xsec_muR20muF10=make_xsecgraph('vh:/muR20muF10')
    g_xsec_muR20muF20=make_xsecgraph('vh:/muR20muF20')

    plottools.graphs([(g_xsec,{'title':'Nominal'}),
                      (g_xsec_muR05muF05,{'title':'#mu_{R}#times0.5, #mu_{F}#times0.5','color':ROOT.Blind1}),
                      (g_xsec_muR05muF10,{'title':'#mu_{R}#times0.5, #mu_{F}#times1.0','color':ROOT.Blind2}),
                      (g_xsec_muR10muF05,{'title':'#mu_{R}#times1.0, #mu_{F}#times0.5','color':ROOT.Blind3}),
                      (g_xsec_muR10muF20,{'title':'#mu_{R}#times1.0, #mu_{F}#times2.0','color':ROOT.Blind4}),
                      (g_xsec_muR20muF10,{'title':'#mu_{R}#times2.0, #mu_{F}#times1.0','color':ROOT.Blind5}),
                      (g_xsec_muR20muF20,{'title':'#mu_{R}#times2.0, #mu_{F}#times2.0','color':ROOT.Blind6})],
                     opt='C',
                     linewidth=2,
                     ratio=0,ratiorange=murrange,
                     xtitle='p^{H}_{T} [GeV]',xrange=(0,1000),
                     ytitle='#sigma(p_{T}>p_{T}^{H}) #times BR [pb]',yrange=yrange,logy=True,
                     text='{}, H#rightarrowbb'.format(title),textpos=(0.55,0.8),
                     legend=(0.2,0.42),legendncol=2,legendwidth=0.2)
    canvastools.save('xsec_mu.pdf')

    #
    # PDF Uncertainty
    g_xsec_pdfs=[(g_xsec,{'title':'NNPDF30_nlo_as_0118','color':ROOT.Blind1})]
    for i in range(1,101):
        g_xsec_pdfs.append((make_xsecgraph('vh:/NNPDF30_nlo_as_0118_{:03d}'.format(i)),{'title':'NNPDF30_nlo_as_0118' if i==0 else '','color':ROOT.Blind1}))

    for i in range(0,33):
        g_xsec_pdfs.append((make_xsecgraph('vh:/PDF4LHC15_nlo_30_pdfas_{:03d}'.format(i)),{'title':'PDF4LHC15_nlo_30_pdfas' if i==0 else '','color':ROOT.Blind2}))

    plottools.graphs(g_xsec_pdfs,
                     opt='C',
                     linewidth=2,
                     ratio=0,ratiorange=pdfrrange,
                     xtitle='p^{H}_{T} [GeV]',xrange=(0,1000),
                     ytitle='#sigma(p_{T}>p_{T}^{H}) #times BR [pb]',yrange=yrange,logy=True,
                     text='{}, H#rightarrowbb, PDF Variations'.format(title),textpos=(0.55,0.8),
                     legend=(0.5,0.75))
    canvastools.save('xsec_pdf.pdf')
