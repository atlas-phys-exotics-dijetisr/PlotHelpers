from prot.utiltools import *
from prot.plottools import *
from prot.filetools import *
from prot.canvastools import *

import ROOT

import dataliketools

import subprocess
import shutil
import glob
import os.path

def plot_filterDPcheck(mR,gSM):
    gSMi=int(gSM*10)
    filetools.filemap('OUT_validate_dijetgamma/hist-MC15.999980.MGPy8EG_dmA_dijetgamma_mR%d_mDM10000_gSM0p%d0_gDM1p50.TRUTH1.root'%(mR,gSMi),'nofilt')
    filetools.filemap('OUT_validate_dijetgamma/hist-MC15.999890.MGPy8EG_dmA_dijetgamma_Ph10_mR%d_mDM10000_gSM0p%d0_gDM1p50.TRUTH1.root'%(mR,gSMi),'mgfilt')
    filetools.filemap('OUT_validate_dijetgamma/hist-MC15.999880.MGPy8EG_dmA_dijetgamma_Ph100_mR%d_mDM10000_gSM0p%d0_gDM1p50.TRUTH1.root'%(mR,gSMi),'athfilt')

    plotsf([('nofilt',{'opt':'hist','title':'No Filter','fillcolor':ROOT.kYellow}),
            ('athfilt',{'title':'100 GeV Athena DP Filter'})],
            'gammajetjet_HLT_g120_ystar/Zprime/mjj',
            hsopt='nostack',
            xrange=(0,1000),legend=(0.5,0.9),text='m_{R} = %d GeV, g_{SM} = %0.1f\nHLT_g120_ystar'%(mR,gSM),textpos=(0.6,0.25),ratio=0,ratiorange=(0.5,1.5))

    save('filt_mjj_mR%d_gSM%0.1f.svg'%(mR,gSM))

def main():
    # Prepare filemaps
    for mR in range(100,1100,100):
        for gSMi in range(1,10):
            gSM=gSMi/10.
            plot_filterDPcheck(mR,gSM)
