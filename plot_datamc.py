import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import style

import selectiontools

import shutil
import copy
import datetime
import os, os.path

def plotCompare(h_mc,h_data,h_sig,histname,**kwargs):
    # style requests
    stylename=histname.split('/')[-1]    
    
    #if 'jet1_eta' not in histname and 'jet1_phi' not in histname: return
    #if 'Zprime_' not in histname: return
    #if 'jet0_pt' not in histname or 'jet0_pt_' in histname: return
    if 'Mjj' in histname: return
    #if 'NumTrk' not in histname: return
    #if 'jet0_pt'!=stylename: return
    #if 'ZprimeISR_dRISRclosej'!=stylename: return

    if stylename in style.style.data: kwargs.update(style.style.data[stylename])

    # histogramming styles
    h_mc [1].update({'opt':'hist',
                     'fillcolor':ROOT.UCYellowLight})
    h_sig[1].update({'opt':'hist',
                     'linecolor':ROOT.kBlue})

    #
    # Settings
    normalize=kwargs.get('normalize',False)
    logy=kwargs.get('logy',False)
    dijetard=kwargs.get('dijetard',False)
    xrange=style.torange(kwargs.get('xrange',(None,None)))
    
    #
    # Automatic pretty

    # Graph type
    direction='<'
    occupancy='<'
    minBin,maxBin=histtools.binrange(h_mc[0],*xrange)
    minBinAb0=max(minBin,h_mc[0].FindFirstBinAbove(0))

    occCentering=h_mc[0].Integral(minBin   ,int((minBin   +maxBin)/2)) / max(h_mc[0].Integral(int((minBin   +maxBin)/2),maxBin),1e-5)    
    if occCentering>1:
        occupancy='>'
    if abs(1-occCentering)<0.3:
        occupancy='='

    dirCentering=h_mc[0].Integral(minBinAb0,int((minBinAb0+maxBin)/2)) / max(h_mc[0].Integral(int((minBinAb0+maxBin)/2),maxBin),1e-5)
    if dirCentering>1:
        direction='>'
    if abs(1-dirCentering)<0.3:
        direction='='
    
    
    # Positions
    textpos=(0.6,0.8)
    legend=(0.17,0.93)
    if occupancy=='>':
        textpos=(0.6,0.8)
        legend=(0.6,0.8)
    elif occupancy=='=':
        textpos=(0.6,0.8)
        legend=(0.17,0.93)

    #
    # Determine yrange by looking at background
    scale=kwargs.get('rebin',1)
    if direction=='=': scale*=1.25
    if direction=='>' and logy and occCentering<2.5: scale*=1.75
    padratio=0.5 if logy else 0.1

    hmaxrange=h_mc[0].Clone()
    hmaxrange.Reset()

    hminrange=h_mc[0].Clone()
    hminrange.Reset()

    integral_mc  =h_mc[0].Integral()
    if integral_mc==0:   integral_mc  =1.

    integral_data=h_data[0].Integral()
    if integral_data==0: integral_data=1.

    integral_sig =h_sig[0].Integral()
    if integral_sig==0:  integral_sig =1.

    scale_mc  =h_mc  [1].get('scale',1) if not normalize else 1./integral_mc
    scale_data=h_data[1].get('scale',1) if not normalize else 1./integral_data
    scale_sig =h_sig [1].get('scale',1) if not normalize else 1./integral_sig

    for binIdx in range(0,hmaxrange.GetNbinsX()+2):
        val_mc  =h_mc  [0].GetBinContent(binIdx)*scale_mc
        val_data=h_data[0].GetBinContent(binIdx)*scale_data
        val_sig =h_sig [0].GetBinContent(binIdx)*scale_sig

        hmaxrange.SetBinContent(binIdx,max([val_mc,val_data,val_sig]))
        hminrange.SetBinContent(binIdx,min([val_mc,val_data,val_sig]))
        
        if dijetard:
            hmaxrange.SetBinContent(binIdx,hmaxrange.GetBinContent(binIdx)/hmaxrange.GetBinWidth(binIdx))
            hminrange.SetBinContent(binIdx,hminrange.GetBinContent(binIdx)/hminrange.GetBinWidth(binIdx))

    # the actual range
    if logy:
        yrange=(prettytools.getminimum(hminrange,*xrange,direction=direction)[1]*(1-padratio)*scale,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)
    else:
        yrange=(0,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)

    print('direction',dirCentering,direction,yrange,1+padratio,scale)
    print('occupancy',occCentering,occupancy,yrange,1+padratio,scale)    
    
    #
    # Determine rebinning based on data
    newbins=None
    if logy and stylename!='Zprime_mjj_var':
        newbins=[]
        rebinNow=1
        rebinIdx=0
        maxdatabin,maxdata=histtools.getmaximum(h_data[0])
        for binIdx in range(1,h_data[0].GetNbinsX()+2):
            if rebinIdx==0:
                newbins.append(h_data[0].GetBinLowEdge(binIdx))

                # Determine new binning
                nowdata=h_data[0].GetBinContent(binIdx)
                rebinNow=min(int(maxdata/nowdata/10)+1 if nowdata>0 else rebinNow,4)
                rebinNow=rebinNow if binIdx>maxdatabin else 1
                rebinIdx=rebinNow-1
            else:
                rebinIdx-=1
        kwargs['rebin']=newbins

    # plot
    hlist=[]
    if h_mc  [0]!=None: hlist.append(h_mc)
    if h_data[0]!=None: hlist.append(h_data)
    if h_sig [0]!=None: hlist.append(h_sig)

    plottools.plots(hlist,
                    hsopt='nostack',
                    ratio=0,ratiolist=[0,1],yrange=yrange,legend=legend,textpos=textpos,**kwargs)
    canvastools.save('%s.pdf'%(os.path.basename(histname)))

def compare(dmc,ddata,dsig,path='',**kwargs):
    oldsavepath=canvastools.savepath
    for key in dmc[0].GetListOfKeys():
        name=key.GetName()
        newpath=path+'/'+name
        obj=key.ReadObj()

        allobjs=[(dmc[0]  .Get(name),dmc[1]  ),
                 (ddata[0].Get(name),ddata[1]),
                 (dsig[0] .Get(name),dsig[1] )]
        
        if obj.InheritsFrom(ROOT.TH1F.Class()):
            canvastools.savepath=oldsavepath
            plotCompare(allobjs[0],allobjs[1],allobjs[2],newpath,**kwargs)
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            if obj.GetName().startswith('sys'): continue
            canvastools.savepath=oldsavepath+'/'+obj.GetName()            
            compare(allobjs[0],allobjs[1],allobjs[2],newpath,**kwargs)
    canvastools.savepath=oldsavepath

def compare_selection(filemc,filedata,filesig,**kwargs):
    # Scale up the background prediction
    jet0_phi_0=filemc  [0].Get('jet0_phi')
    jet0_phi_1=filedata[0].Get('jet0_phi')

    ratio=jet0_phi_1.Clone()
    ratio.Scale(1./filemc[1].get('scale',1.))
    ratio.Divide(jet0_phi_0)

    fr=ratio.Fit('pol0','s')
    normFactor=fr.Parameter(0)
    filemc[1]['scale']=filemc[1].get('scale',1)*normFactor

    fh_info=open(canvastools.savefullpath(True)+'/info.txt','w')
    fh_info.write('normFactor = %0.2f\n'%normFactor)
    fh_info.close()

    # start plotting
    compare(filemc,filedata,filesig,path=filemc[0].GetName(),**kwargs)

def main(file0,file1,files,**kwargs):
    canvastools.savesetup('datavsmc',**kwargs)

    # Prepare inputs
    file0=utiltools.filetag(file0)
    file1=utiltools.filetag(file1)
    files=utiltools.filetag(files)

    filetools.filemap(file0[0],'file0')
    filetools.filemap(file1[0],'file1')
    filetools.filemap(files[0],'files')

    fh=utiltools.Get('file0:/')

    # Add lumi scaling
    lumi=kwargs.get('lumi',1.)
    file0[1]['scale']=file0[1].get('scale',1,)*lumi*1e3
    files[1]['scale']=files[1].get('scale',1,)*lumi*1e3

    #
    selectiontools.loop(compare_selection,file0,file1,files,**kwargs)

    canvastools.savereset()
