#
# Generates datalike distributions using fits

from prot import filetools
from prot import fittools
from prot import canvastools
from prot import batchtools
from prot import plottools

import ROOT

import DijetFuncs

import dataliketools
import selectiontools

import subprocess
import shutil
import glob
import os.path

def makeFitDataLike(d_bkg,funcName='CDF',massRange=(570,1200),**kwargs):
    print('=====',d_bkg[0].GetName())
    if d_bkg[0].GetName() not in ['dibjet60_blinded','dibjet70_blinded','dibjet77_blinded','dibjet85_blinded']: return
    # Prepare fit function
    func=DijetFuncs.getFit(funcName)
    func.resetTF1()    
    mjj=d_bkg[0].Get('Zprime_mjj')
    fittools.fit(mjj,func.TF1,fitrange=massRange,logy=True,
                 text={'sim':False,
                       'lumi':3.2,
                       'title':'%s, %s Func'%(d_bkg[0].GetName(),func.fDisplayNames)})
    canvastools.save('fitDataLike/fit%s.pdf'%funcName)

    # Make new data
    h_data=ROOT.TH1F('data',mjj.GetTitle(),63,massRange[0],massRange[1])
    #h_data.FillRandom('STD',int(STD.TF1.Integral(570,1000)/h_data.GetBinWidth(1)))

    # dijetard definition
    for binIdx in range(1,h_data.GetNbinsX()+1):
        nEvents=func.TF1.Integral(h_data.GetBinLowEdge(binIdx),h_data.GetBinLowEdge(binIdx+1))/h_data.GetBinWidth(binIdx)
        print(binIdx,func.TF1.Eval(h_data.GetBinCenter(binIdx)),h_data.GetBinWidth(binIdx))
        h_data.SetBinContent(binIdx,nEvents)

    # Save new data
    if 'outFile' in kwargs:
        outFile=kwargs['outFile']
        outFile.cd()
        h_data.Write()

def main(bkgpath,outName='datalike'):
    fh_bkg=filetools.open(bkgpath)
    selectiontools.loop(makeFitDataLike,fh_bkg,outFile=os.path.dirname(bkgpath)+'/datalike.root')
