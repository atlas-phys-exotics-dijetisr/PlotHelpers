import ROOT

from prot import filetools
from prot import canvastools
from prot import utiltools

import fnmatch

titles={}
titles['dijetgamma_g130_2j25']='X + #gamma (P_{T,#gamma}>130 GeV)'
titles['dijetgamma_g150_2j25']='X + #gamma (P_{T,#gamma}>150 GeV)'
titles['trijet_j430_2j25']='X + j (P_{T,j}>430 GeV)'

def loop(function,*files,**kwargs):
    fh_ref=filetools.open(utiltools.filetag(files[0])[0])

    # Outputs
    outFile=None
    if 'outFile' in kwargs:
        outFile=filetools.open(kwargs.get('outFile'),'RECREATE')
    savepath=canvastools.savepath

    # Loop
    for key in fh_ref.GetListOfKeys():
        if not key.ReadObj().InheritsFrom(ROOT.TDirectoryFile.Class()): continue
        if 'selection' in kwargs and not fnmatch.fnmatch(key.GetName(),kwargs['selection']): continue

        # Prepare output dir
        if outFile!=None:
            d_out=outFile.mkdir(key.GetName())
            kwargs['outFile']=d_out
        
        # Prepare input dirs
        newargs=[]
        for path in files:
            newarg=None
            if type(path)==list:
                newarg=[]
                for apath in path:
                    apath=utiltools.filetag(apath)
                    newarg.append((filetools.open(apath[0]).Get(key.GetName()),apath[1]))
            else:
                path=utiltools.filetag(path)
                newarg=(filetools.open(path[0]).Get(key.GetName()),path[1])
            newargs.append(newarg)

        canvastools.savepath=savepath+'/'+key.GetName()
        function(*newargs,**kwargs)
    
    canvastools.savepath=savepath
