from prot import plottools
from prot import filetools
from prot import canvastools
from prot import utiltools
from prot import fittools

import ROOT

from math import *

def print_row(selection):
    h_sig=utiltools.Get('z:/{}/Hcand_m'.format(selection))
    h_bkg=utiltools.Get('qcd:/{}/Hcand_m'.format(selection))

    r=fittools.fit(h_sig,'gaus',fitrange=(70,110),xrange=(0,150),fitopt='q')
    print('Mean',r.Parameter(1))
    print('StdDev',r.Parameter(2))

    s=h_sig.Integral(h_sig.FindBin(70),h_sig.FindBin(110))
    b=h_bkg.Integral(h_bkg.FindBin(70),h_bkg.FindBin(110))
    soversqrtb=s/sqrt(b)
    print('s/sqrt(b)',soversqrtb)

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','qcd')
    filetools.filemap('OUT_fatjet_mc/hist-z-Sherpa225.root','z')

    print_row('hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77')
    print_row('hbbisrsd_GhostVR30Rmax4Rmin02TrackJetGhostTag_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77')
    print_row('hbbisrsd_GhostVR30Rmax4Rmin02TrackJetGhostTag_fj0pt480_fj1pt250_tjet2_skipVRcontain_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77')
