from prot import plottools
from prot import filetools
from prot import canvastools

import ROOT

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','qcd')
    filetools.filemap('OUT_fatjet_mc/hist-z-Sherpa225.root','z')

    plottools.plotsf([('qcd:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Trimmed',
                                                                                                                                                          'color':ROOT.Blind0}),
                      ('qcd:/hbbisrsd_GhostVR30Rmax4Rmin02TrackJetGhostTag_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Soft-Drop',
                                                                                                                                                                    'color':ROOT.Blind2}),
                      ('qcd:/hbbisrsd_GhostVR30Rmax4Rmin02TrackJetGhostTag_fj0pt480_fj1pt250_tjet2_skipVRcontain_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Soft-Drop (Allow Overlap)',
                                                                                                                                                                                  'color':ROOT.Blind3})],
                     'Hcand_m',
                     hsopt='nostack',opt='hist',
                     xrange=(0,250),
                     ratio=0,ratiorange=(0,2),
                     ytitle='Events',
                     legend=(0.25,0.4),text='SR, Pythia 8 QCD',textpos=(0.5,0.75),lumi=80.5)
    canvastools.save('softdrop/currentdiBjet_qcd.pdf')

    plottools.plotsf([('z:/hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Trimmed',
                                                                                                                                                        'color':ROOT.Blind0}),
                      ('z:/hbbisrsd_GhostVR30Rmax4Rmin02TrackJetGhostTag_fj0pt480_fj1pt250_tjet2_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Soft-Drop',
                                                                                                                                                                  'color':ROOT.Blind2}),
                      ('z:/hbbisrsd_GhostVR30Rmax4Rmin02TrackJetGhostTag_fj0pt480_fj1pt250_tjet2_skipVRcontain_ptsort0_vetottbarCR_hpt480_nbtag2in2ext_MV2c10_FixedCutBEff_77',{'title':'Soft-Drop (Allow Overlap)',
                                                                                                                                                                                'color':ROOT.Blind3})],
                     'Hcand_m',
                     hsopt='nostack',opt='hist',
                     xrange=(0,250),
                     ratio=0,ratiorange=(0,2),
                     ytitle='Events',
                     legend=(0.5,0.5),text='SR, Sherpa 2.2.5 Z+jets',textpos=(0.5,0.75),lumi=80.5)
    canvastools.save('softdrop/currentdiBjet_z.pdf')
