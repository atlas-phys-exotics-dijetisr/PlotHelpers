#!/bin/bash

if [ ${#} != 1 ] && [ ${#} != 2 ]; then
    echo "usage: ${0} sample [wildcard]"
    exit -1
fi

SAMPLE=${1}
if [ ${#} == 2 ]; then
    WILDCARD=${2}
else
    WILDCARD="*"
fi

DATADIR=/global/projecta/projectdirs/atlas/${USER}/batch

rsync -av --delete pdsf.nersc.gov:${DATADIR}/${SAMPLE}/hist${WILDCARD}root ${SAMPLE}/
