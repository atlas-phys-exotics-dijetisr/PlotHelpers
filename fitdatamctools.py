import array

import ROOT

ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize")
ROOT.Math.MinimizerOptions.SetDefaultMaxIterations(1000000)
ROOT.Math.MinimizerOptions.SetDefaultMaxFunctionCalls(100000)
ROOT.Math.MinimizerOptions.SetDefaultTolerance(0.1)

class FitFunction(ROOT.TPyMultiGenFunction):
    def __init__(self,mchists,datahist):
        ROOT.TPyMultiGenFunction.__init__(self, self)

        self.mchists=mchists
        self.datahist=datahist

        self.binmin=datahist.FindBin(70)
        self.binmax=datahist.FindBin(230)

    def NDim(self):
        return len(self.mchists)

    def DoEval(self, args):
        totalchi2=0.
        print('start',args[0])
        for binidx in range(self.binmin,self.binmax+1):
            data=self.datahist.GetBinContent(binidx)
            err=self.datahist.GetBinError(binidx)
            mc=0
            for mcidx in range(len(self.mchists)):
                mchist=self.mchists[mcidx]
                if type(mchist)==tuple:
                    for mcsubhist in mchist:
                        mc+=args[mcidx]*mcsubhist.GetBinContent(binidx)
                else:
                    mc+=args[mcidx]*mchist.GetBinContent(binidx)

            chi2=(data-mc)**2/err**2
            totalchi2+=chi2
        print(totalchi2)
        return totalchi2

def fitDataMC(hs_ms,h_data,fix=None):
    thisFunctor=FitFunction(hs_ms,h_data)

    #
    # Create a fitter
    thisFitter = ROOT.Fit.Fitter()
    thisFitter.Config().MinimizerOptions().SetMinimizerType("Minuit2")
    thisFitter.Config().MinimizerOptions().SetMinimizerAlgorithm("Minimize")
    thisFitter.Config().MinimizerOptions().SetMaxIterations(1000000)
    thisFitter.Config().MinimizerOptions().SetMaxFunctionCalls(100000)
    thisFitter.Config().MinimizerOptions().SetStrategy(1)

    # Setup parameters
    aParams = array.array( 'd', [1.]*thisFunctor.NDim())
    aSteps  = array.array( 'd', [0.01]*thisFunctor.NDim())
    thisFitter.Config().SetParamsSettings(thisFunctor.NDim(), aParams, aSteps)

    if fix!=None:
        for parIdx in fix:
            thisFitter.Config().ParamsSettings()[parIdx].Fix()
    
    #
    # Run the fit
    thisFitter.FitFCN(thisFunctor)
    result=ROOT.TFitResult(thisFitter.Result())
    result.Print()
    return result
