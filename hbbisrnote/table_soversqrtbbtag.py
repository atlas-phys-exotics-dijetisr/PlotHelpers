from prot import filetools
from prot import utiltools
from prot import textools

import ROOT

from math import *

from hbbisrnote import constants

def calc_soversqrtb(tagger,fh_tex):
    print(tagger)
    selection=constants.selectionSR.replace('MV2c10_FixedCutBEff_77',tagger)
    print(selection)
    s=utiltools.Get('sig:/{}/M100to150/Hcand_m'.format(selection)).Integral()
    b=utiltools.Get('bkg:/{}/M100to150/Hcand_m'.format(selection)).Integral()

    sgn=s/sqrt(b)
    
    print(selection,sgn)
    textools.write_command(fh_tex,'BTagOptim{}'.format(tagger),sgn,fmt=':0.2f')

def main():
    filetools.filemap('OUT_fatjet_mc/hist-sig.root','sig')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root','bkg')

    fh_tex=open('btagsoversqrtb.tex','w')

    calc_soversqrtb('MV2c10_FixedCutBEff_60',   fh_tex=fh_tex)
    calc_soversqrtb('MV2c10_FixedCutBEff_70',   fh_tex=fh_tex)
    calc_soversqrtb('MV2c10_FixedCutBEff_77',   fh_tex=fh_tex)
    calc_soversqrtb('MV2c10_FixedCutBEff_85',   fh_tex=fh_tex)

    calc_soversqrtb('MV2c10mu_FixedCutBEff_60', fh_tex=fh_tex)
    calc_soversqrtb('MV2c10mu_FixedCutBEff_70', fh_tex=fh_tex)
    calc_soversqrtb('MV2c10mu_FixedCutBEff_77', fh_tex=fh_tex)
    calc_soversqrtb('MV2c10mu_FixedCutBEff_85', fh_tex=fh_tex)

    calc_soversqrtb('MV2c10rnn_FixedCutBEff_60',fh_tex=fh_tex)
    calc_soversqrtb('MV2c10rnn_FixedCutBEff_70',fh_tex=fh_tex)
    calc_soversqrtb('MV2c10rnn_FixedCutBEff_77',fh_tex=fh_tex)
    calc_soversqrtb('MV2c10rnn_FixedCutBEff_85',fh_tex=fh_tex)

    calc_soversqrtb('DL1_FixedCutBEff_60',      fh_tex=fh_tex)
    calc_soversqrtb('DL1_FixedCutBEff_70',      fh_tex=fh_tex)
    calc_soversqrtb('DL1_FixedCutBEff_77',      fh_tex=fh_tex)
    calc_soversqrtb('DL1_FixedCutBEff_85',      fh_tex=fh_tex)

    calc_soversqrtb('DL1mu_FixedCutBEff_60',    fh_tex=fh_tex)
    calc_soversqrtb('DL1mu_FixedCutBEff_70',    fh_tex=fh_tex)
    calc_soversqrtb('DL1mu_FixedCutBEff_77',    fh_tex=fh_tex)
    calc_soversqrtb('DL1mu_FixedCutBEff_85',    fh_tex=fh_tex)

    calc_soversqrtb('DL1rnn_FixedCutBEff_60',   fh_tex=fh_tex)
    calc_soversqrtb('DL1rnn_FixedCutBEff_70',   fh_tex=fh_tex)
    calc_soversqrtb('DL1rnn_FixedCutBEff_77',   fh_tex=fh_tex)
    calc_soversqrtb('DL1rnn_FixedCutBEff_85',   fh_tex=fh_tex)

    fh_tex.close()
