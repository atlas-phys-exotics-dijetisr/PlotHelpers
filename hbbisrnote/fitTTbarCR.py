from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools

import fitdatamctools

from hbbisrnote import constants

import ROOT

def main():
    #
    # Create histograms
    fh_w        =filetools.open('OUT_fatjet_mc/hist-w.root')
    fh_z        =filetools.open('OUT_fatjet_mc/hist-z.root')
    fh_qcd      =filetools.open('OUT_fatjet_mc/hist-qcd.root')
    fh_ttbar    =filetools.open('OUT_fatjet_mc/hist-ttbar.root')
    fh_singletop=filetools.open('OUT_fatjet_mc/hist-singletop.root')
    fh_wlnu     =filetools.open('OUT_fatjet_mc/hist-Wlnu.root')
    fh_data     =filetools.open('OUT_fatjet_data/hist-data.root')

    h_w        =fh_w        .Get('{}/Hcand_m'.format(constants.selectionTTbarCR))
    h_z        =fh_z        .Get('{}/Hcand_m'.format(constants.selectionTTbarCR))
    h_qcd      =fh_qcd      .Get('{}/Hcand_m'.format(constants.selectionTTbarCR))
    h_ttbar    =fh_ttbar    .Get('{}/Hcand_m'.format(constants.selectionTTbarCR))
    h_singletop=fh_singletop.Get('{}/Hcand_m'.format(constants.selectionTTbarCR))
    h_wlnu     =fh_wlnu     .Get('{}/Hcand_m'.format(constants.selectionTTbarCR))
    h_data     =fh_data     .Get('{}/Hcand_m'.format(constants.selectionTTbarCR))

    print('w'        ,h_w        .Integral())
    print('z'        ,h_z        .Integral())
    print('qcd'      ,h_qcd      .Integral())
    print('ttbar'    ,h_ttbar    .Integral())
    print('singletop',h_singletop.Integral())
    print('wlnu'     ,h_wlnu     .Integral())
    
    # output
    fh_fitTTbarCR=open('fitTTbarCR.tex','w')
    
    #
    # The unfit plot
    plottools.plots([(h_w,{'title':'W#rightarrowqq',
                           'fillcolor':ROOT.kBlue,
                           'opt':'hist'}),
                     (h_z,{'title':'Z#rightarrowqq',
                           'fillcolor':ROOT.kRed,
                           'opt':'hist'}),
                     (h_qcd,{'title':'QCD',
                             'fillcolor':ROOT.UCYellow,
                             'opt':'hist'}),
                     (h_wlnu,{'title':'W#rightarrow l#nu',
                                  'fillcolor':ROOT.kMagenta,
                                  'opt':'hist'}),                                                  
                     (h_singletop,{'title':'Wt',
                                   'fillcolor':ROOT.kGreen+4,
                                   'opt':'hist'}),                     
                     (h_ttbar,{'title':'t#bar{t}',
                               'fillcolor':ROOT.kGreen+2,
                               'opt':'hist'}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2,3,4,5),6),
                    text='CRqcd - No Fit',lumi=constants.lumi,sim=False,textpos=(0.2,0.8),
                    legend=(0.6,0.93),legendncol=2,
                    logy=False,yrange=(1e-1,1e3),ytitle='Events',
                    xrange=(60,200),
                    ratio=0,ratiorange=(0.,2.),ratiotitle='Data/MC')
    canvastools.save('{}/fitTTbarCR/fixall_Hcand_m.pdf'.format(constants.selectionTTbarCR))

    #
    # Fit with everything float
    result=fitdatamctools.fitDataMC([h_singletop,h_ttbar,h_wlnu],h_data,fix=[0,2])
    plottools.plots([(h_wlnu,{'title':'W#rightarrow l#nu',
                                  'fillcolor':ROOT.kMagenta,
                                  'opt':'hist'}),
                                  (h_singletop,{'title':'Wt',
                                   'fillcolor':ROOT.kGreen+4,
                                   'opt':'hist'}),
                     (h_ttbar,{'title':'t#bar{{t}} (#mu={0:0.2f} #pm {1:0.2f})'.format(result.Parameter(1),result.ParError(1)),
                                  'fillcolor':ROOT.kGreen+2,
                                  'opt':'hist',
                                  'scale':result.Parameter(1)}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2),3),
                    text='CRttbar - Float All',lumi=constants.lumi,sim=False,textpos=(0.2,0.8),
                    legend=(0.5,0.93),legendncol=2,
                    logy=False,yrange=(0,120),ytitle='Events',
                    xrange=(60,300),
                    ratio=0,ratiorange=(0.,2.),ratiotitle='Data/MC')
    canvastools.save('{}/fitTTbarCR/floatall_Hcand_m.pdf'.format(constants.selectionTTbarCR))

    textools.write_command(fh_fitTTbarCR,'FitTTbarCRFloatAllMuTTbar',result.Parameter(2),fmt=':0.01f')
    textools.write_command(fh_fitTTbarCR,'FitTTbarCRFloatAllMuTTbarErr',result.ParError(2),fmt=':0.01f')
    
    #
    #
    fh_fitTTbarCR.close()
