import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import style

import selectiontools

from hbbisrnote import constants

import copy
import shutil
import copy
import datetime
import os, os.path

from math import *

def plotCompare(h_data,hs_bkgs,histname,**kwargs):
    if histname.startswith('/jet'): return
    if histname[1:] not in ['mu_ave','NPV',
                            'HcandIdx',
                            'Hcand_m','Hcand_pt_m','Hcand_phi','Hcand_eta',
                            'Hcand_D2','hbb_dRminR',
                            'trkjet0_pt','trkjet1_pt','trkjet2_pt',
                            'nFatJets','nTrkJets',
                            'fatjet0_m','fatjet0_pt_m','fatjet0_phi','fatjet0_eta','fatjet0_D2',
                            'fatjet1_m','fatjet1_pt_m','fatjet1_phi','fatjet1_eta','fatjet1_D2']: return
    #if histname[1:] not in ['Hcand_m']: return

    if type(hs_bkgs)!=list: hs_bkgs=[hs_bkgs]
        
    # style requests
    stylename=histname.split('/')[-1]    
    
    if stylename in style.style.data: kwargs.update(style.style.data[stylename])

    # histogramming styles
    hs_bkgs=copy.deepcopy(hs_bkgs)
    for i in range(len(hs_bkgs)):
        hs_bkgs[i][1].update({'opt':'hist',
                              'scale':hs_bkgs[i][1].get('scale',1)})

    h_bkg=histtools.mergestack(list(map(lambda h: h[0],hs_bkgs)),
                               scale=list(map(lambda h: h[1].get('scale',1),hs_bkgs)))

    #
    # Settings
    normalize=kwargs.get('normalize',False)
    logy=kwargs.get('logy',False)
    xrange=style.torange(kwargs.get('xrange',(None,None)))

    #
    # Automatic pretty

    # Graph type
    direction='<'
    occupancy='<'
    minBin,maxBin=histtools.binrange(h_data[0],*xrange)
    minBinAb0=max(minBin,h_data[0].FindFirstBinAbove(0))

    occCentering=h_data[0].Integral(minBin   ,int((minBin   +maxBin)/2)) / max(h_data[0].Integral(int((minBin   +maxBin)/2),maxBin),1e-5)    
    if occCentering>1:
        occupancy='>'
    if abs(1-occCentering)<0.3:
        occupancy='='

    dirCentering=h_data[0].Integral(minBinAb0,int((minBinAb0+maxBin)/2)) / max(h_data[0].Integral(int((minBinAb0+maxBin)/2),maxBin),1e-5)
    if dirCentering>1:
        direction='>'
    if abs(1-dirCentering)<0.3:
        direction='='
    
    
    # Positions
    scale=kwargs.get('rebin',1)
    textpos=(0.6,0.8)
    legend=(0.17,0.93)
    if occupancy=='>':
        textpos=(0.6,0.8)
        legend=(0.6,0.75)
    if occupancy=='=': # Flat situation
        scale*=1.25
        textpos=(0.6,0.8)
        legend=(0.17,0.93)
    if logy: # log is usually mostly flat situation
        scale*=1.25
        textpos=(0.2,0.8)
        legend=(0.6,0.93)

    #
    # Determine yrange by looking at background
    padratio=0.5 if logy else 0.1

    hmaxrange=h_data[0].Clone()
    hmaxrange.Reset()

    hminrange=h_data[0].Clone()
    hminrange.Reset()

    integral_bkg =h_bkg    .Integral()
    if integral_bkg ==0: integral_bkg =1.

    integral_data=h_data[0].Integral()
    if integral_data==0: integral_data=1.

    scale_bkg =1.                       if not normalize else 1./integral_bkg
    scale_data=h_data[1].get('scale',1) if not normalize else 1./integral_data

    for binIdx in range(0,hmaxrange.GetNbinsX()+2):
        val_bkg =h_bkg        .GetBinContent(binIdx)*scale_bkg
        val_bkg0=hs_bkgs[0][0].GetBinContent(binIdx)*scale_bkg
        val_data=h_data[0]    .GetBinContent(binIdx)*scale_data

        hmaxrange.SetBinContent(binIdx,max([val_bkg,val_data]))
        hminrange.SetBinContent(binIdx,min([val_bkg,val_bkg0,val_data]))

    # the actual range
    if logy:
        ymin=prettytools.getminimum(hminrange,*xrange,direction=direction)[1]
        ymax=prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)
        ymax=pow(10,log(ymin,10)+(log(ymax,10)-log(ymin,10))*scale)
        yrange=(ymin,ymax)

    else:
        yrange=(0,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)

    print('direction',dirCentering,direction,yrange,1+padratio,scale)
    print('occupancy',occCentering,occupancy,yrange,1+padratio,scale)    

    #
    # plot
    stackgroup=[]
    hlist=[]
    hlist+=hs_bkgs
    if len(hs_bkgs)>1: stackgroup.append(tuple(range(len(hs_bkgs))))
    else: stackgroup.append(0)
    hlist.append(h_data )
    stackgroup.append(len(hlist)-1)

    stackgroup=tuple(stackgroup)

    if 'ratiorange' not in kwargs:
        h_datacopy=h_data[0].Clone()
        h_datacopy.Divide(h_bkg)
        print(prettytools.getminimum(h_datacopy,*xrange))
        print(prettytools.getmaximum(h_datacopy,*xrange))
        ratmin=prettytools.getminimum(h_datacopy,*xrange)[1]
        ratmax=prettytools.getmaximum(h_datacopy,*xrange)[1]
        ratran=min(max(abs(1-ratmin),abs(1-ratmax)),0.5)*1.5
        kwargs['ratiorange']=(1-ratran,1+ratran)

    plottools.plots(hlist,
                    hsopt='nostack',stackgroup=stackgroup,
                    ratio=0,ratiolist=[0,1],ratiotitle='Data/MC',ratiobaseerror=True,
                    xtitle=h_data[0].GetXaxis().GetTitle().replace('Z\'','signal'),
                    yrange=yrange,ytitle='events',
                    legend=legend,legendncol=2,
                    text='SR',textpos=textpos,sim=False,
                    **kwargs)
    canvastools.save('%s.pdf'%(os.path.basename(histname)))

def compare_selection(ddata,dw,dz,dttbar,dqcd,path='',**kwargs):
    oldsavepath=canvastools.savepath
    for key in ddata[0].GetListOfKeys():
        name=key.GetName()
        newpath=path+'/'+name
        obj=key.ReadObj()

        data= (ddata[0]     .Get(name),ddata     [1])
        bkg =[(dw[0]        .Get(name),dw        [1]),
              (dz[0]        .Get(name),dz        [1]),
              (dttbar[0]    .Get(name),dttbar    [1]),
              (dqcd[0]      .Get(name),dqcd      [1]),]
        
        if obj.InheritsFrom(ROOT.TH1F.Class()):
            canvastools.savepath=oldsavepath
            plotCompare(data,bkg,newpath,**kwargs)
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            if obj.GetName().startswith('sys'): continue
            if obj.GetName().startswith('M'): continue
            continue # Disable all subfolders for now
            canvastools.savepath=oldsavepath+'/'+obj.GetName()            
            compare_selection(allobjs[0],allobjs[1],allobjs[2],newpath,**kwargs)
    canvastools.savepath=oldsavepath

def main(**kwargs):
    canvastools.savesetup('datavsmc',**kwargs)

    # Prepare inputs
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'  ,'qcd')
    filetools.filemap('OUT_fatjet_mc/hist-w.root'    ,'w')
    filetools.filemap('OUT_fatjet_mc/hist-z.root'    ,'z')
    filetools.filemap('OUT_fatjet_mc/hist-ttbar.root','ttbar')
    filetools.filemap('OUT_fatjet_data/hist-data.root' ,'data')
    filetools.filemap('OUT_fatjet_mc/hist-sig.root'  ,'sig')

    qcd      =('qcd'      ,{'title':'QCD',
                            'scale':constants.qcdscaleSR,
                            'fillcolor':ROOT.UCYellow})
    w        =('w'        ,{'title':'W',
                            'fillcolor':ROOT.kBlue})
    z        =('z'        ,{'title':'Z',
                            'fillcolor':ROOT.kRed})
    ttbar    =('ttbar'    ,{'title':'t#bar{t}',
                            'fillcolor':ROOT.kGreen+2})
    data     =('data'     ,{'title':'Data'})

    #
    selectiontools.loop(compare_selection,data,w,z,ttbar,qcd,selection=constants.selectionSR,lumi=constants.lumi,**kwargs)

    canvastools.savereset()
