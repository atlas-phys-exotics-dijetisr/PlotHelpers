import ROOT
import re

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools
from prot import style

import selectiontools
import optimtools

from hbbisrnote import constants

from math import *

def main(**kwargs):
    canvastools.savesetup('ttbarsoversqrtb',**kwargs)

    filetools.filemap('OUT_fatjet_mc/hist-ttbar.root','ttbar')
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'  ,'qcd')

    selectiontools.loop(optimtools.runAllCutStudy,'qcd','ttbar',selection='hbbisr_GhostVR30Rmax4Rmin02TrackJet_fj0pt480_fj1pt250_tjet2_ptsort0_hpt480_hm40_nbtag1in2ext_MV2c10_FixedCutBEff_77',massdir='',sigtitle='t#bar{t} (sig)',bkgtitle='QCD (bkg)',**kwargs)

    canvastools.savereset()
