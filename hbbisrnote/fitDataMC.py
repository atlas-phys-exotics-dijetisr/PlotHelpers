from prot import filetools
from prot import plottools
from prot import canvastools
from prot import textools

import fitdatamctools

from hbbisrnote import constants

import ROOT

def main():
    #
    # Create histograms
    fh_w        =filetools.open('OUT_fatjet_mc/hist-mc16a-w.root')
    fh_z        =filetools.open('OUT_fatjet_mc/hist-mc16a-z.root')
    fh_qcd      =filetools.open('OUT_fatjet_mc/hist-mc16a-qcd.root')
    fh_ttbar    =filetools.open('OUT_fatjet_mc/hist-mc16a-ttbar.root')
    fh_singletop=filetools.open('OUT_fatjet_mc/hist-mc16a-singletop.root')
    fh_data     =filetools.open('OUT_fatjet_data/hist-data1516.root') #OUT_fatjet_data/hist-data15.root')

    h_w        =fh_w        .Get('{}/Hcand_m'.format(constants.selectionCR.replace('_hm40','')))
    h_z        =fh_z        .Get('{}/Hcand_m'.format(constants.selectionCR.replace('_hm40','')))
    h_qcd      =fh_qcd      .Get('{}/Hcand_m'.format(constants.selectionCR.replace('_hm40','')))
    h_ttbar    =fh_ttbar    .Get('{}/Hcand_m'.format(constants.selectionCR.replace('_hm40','')))
    h_singletop=fh_singletop.Get('{}/Hcand_m'.format(constants.selectionCR.replace('_hm40','')))
    h_data     =fh_data     .Get('{}/Hcand_m'.format(constants.selectionCR.replace('_hm40','')))

    # output
    fh_fitDataMC=open('fitDataMC.tex','w')

    #
    # The unfit plot
    plottools.plots([(h_w,{'title':'W',
                           'fillcolor':ROOT.kBlue,
                           'opt':'hist'}),
                     (h_z,{'title':'Z',
                           'fillcolor':ROOT.kRed,
                           'opt':'hist'}),
                     (h_ttbar,{'title':'t#bar{t}',
                               'fillcolor':ROOT.kGreen+2,
                               'opt':'hist'}),
                     (h_qcd,{'title':'QCD',
                             'fillcolor':ROOT.UCYellow,
                             'opt':'hist',}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2,3),4),
                    text='CRqcd - No Fit',lumi=constants.lumi1516,sim=False,textpos=(0.2,0.8),
                    legend=(0.6,0.93),legendncol=2,
                    logy=True,yrange=(1e2,1e7),ytitle='Events',
                    xrange=(70,230),
                    ratio=0,ratiorange=(0.7,0.8),ratiotitle='Data/MC')
    canvastools.save('{}/fitdatamc/fixall_Hcand_m.pdf'.format(constants.selectionCR))

    #
    # Fit with everything float
    result=fitdatamctools.fitDataMC([h_qcd,(h_w,h_z),h_ttbar],h_data)
    plottools.plots([(h_w,{'title':'W (#mu={0:0.2f} #pm {1:0.2f})'.format(result.Parameter(1),result.ParError(1)),
                           'fillcolor':ROOT.kBlue,
                           'opt':'hist',
                           'scale':result.Parameter(1)}),
                     (h_z,{'title':'Z (#mu={0:0.2f} #pm {1:0.2f})'.format(result.Parameter(1),result.ParError(1)),
                           'fillcolor':ROOT.kRed,
                           'opt':'hist',
                           'scale':result.Parameter(1)}),
                     (h_ttbar,{'title':'t#bar{{t}} (#mu={0:0.2f} #pm {1:0.1f})'.format(result.Parameter(2),result.ParError(2)),
                               'fillcolor':ROOT.kGreen+2,
                               'opt':'hist',
                               'scale':result.Parameter(2)}),
                     (h_qcd,{'title':'QCD (#mu={0:0.4f} #pm {1:0.4f})'.format(result.Parameter(0),result.ParError(0)),
                             'fillcolor':ROOT.UCYellow,
                             'opt':'hist',
                             'scale':result.Parameter(0)}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2,3),4),
                    text='CRqcd - Float All',lumi=constants.lumi1516,sim=False,textpos=(0.2,0.8),
                    legend=(0.5,0.93),legendncol=2,
                    logy=True,yrange=(1e2,1e7),ytitle='Events',
                    xrange=(70,230),
                    ratio=0,ratiorange=(0.95,1.05),ratiotitle='Data/MC')
    canvastools.save('{}/fitdatamc/floatall_Hcand_m.pdf'.format(constants.selectionCR))

    textools.write_command(fh_fitDataMC,'FitDataMCFloatAllMuV',result.Parameter(1),fmt=':0.02f')
    textools.write_command(fh_fitDataMC,'FitDataMCFloatAllMuVErr',result.ParError(1),fmt=':0.02f')

    textools.write_command(fh_fitDataMC,'FitDataMCFloatAllMuTTbar',result.Parameter(2),fmt=':0.01f')
    textools.write_command(fh_fitDataMC,'FitDataMCFloatAllMuTTbarErr',result.ParError(2),fmt=':0.01f')

    textools.write_command(fh_fitDataMC,'FitDataMCFloatAllMuQCD',result.Parameter(0),fmt=':0.04f')
    textools.write_command(fh_fitDataMC,'FitDataMCFloatAllMuQCDErr',result.ParError(0),fmt=':0.04f')

    #
    # Fit with everything float
    result=fitdatamctools.fitDataMC([h_qcd,(h_w,h_z),h_ttbar],h_data,fix=[1,2])
    plottools.plots([(h_w,{'title':'W',
                           'fillcolor':ROOT.kBlue,
                           'opt':'hist'}),
                     (h_z,{'title':'Z',
                           'fillcolor':ROOT.kRed,
                           'opt':'hist'}),
                     (h_ttbar,{'title':'t#bar{t}',
                               'fillcolor':ROOT.kGreen+2,
                               'opt':'hist'}),
                     (h_qcd,{'title':'QCD (#mu={0:0.4f} #pm {1:0.4f})'.format(result.Parameter(0),result.ParError(0)),
                             'fillcolor':ROOT.UCYellow,
                             'opt':'hist',
                             'scale':result.Parameter(0)}),
                     (h_data,{'title':'Data'})],
                    hsopt='nostack',stackgroup=((0,1,2,3),4),
                    text='CRqcd - Float QCD',lumi=constants.lumi1516,sim=False,textpos=(0.2,0.8),
                    legend=(0.5,0.93),legendncol=2,
                    logy=True,yrange=(1e2,1e7),ytitle='Events',
                    xrange=(70,230),
                    ratio=0,ratiorange=(0.95,1.05),ratiotitle='Data/MC')
    canvastools.save('{}/fitdatamc/floatqcd_Hcand_m.pdf'.format(constants.selectionCR))

    textools.write_command(fh_fitDataMC,'FitDataMCFloatQCDMuQCD',result.Parameter(0),fmt=':0.04f')
    textools.write_command(fh_fitDataMC,'FitDataMCFloatQCDMuQCDErr',result.ParError(0),fmt=':0.04f')
    
    #
    #
    fh_fitDataMC.close()
