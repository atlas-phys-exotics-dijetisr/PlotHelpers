import ROOT
from prot import utiltools
from prot import filetools
from prot import textools
from prot import histtools

from hbbisrnote import constants

def generate_table(fpath,selection,selectioncode,scale=1.,fhout=None):
    if type(fpath) is not list:
        fpath=[fpath]
        scale=[scale]
    cutflow=histtools.mergestack([utiltools.Get('{}:/{}/cutflow_weighted'.format(fp,selection)) for fp in fpath], scale=scale)

    for binidx in range(1,cutflow.GetNbinsX()+2):
        text=cutflow.GetXaxis().GetBinLabel(binidx)
        if text=='': continue
        value=cutflow.GetBinContent(binidx)
        print(text,cutflow.GetBinContent(binidx))
        textools.write_command(fhout, 'Cutflow{}{}'.format(selectioncode,text), value, fmt=':0.0f' )

def main():
    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'       ,'qcd')
    filetools.filemap('OUT_fatjet_mc/hist-w.root'         ,'w')
    filetools.filemap('OUT_fatjet_mc/hist-z.root'         ,'z')
    filetools.filemap('OUT_fatjet_mc/hist-ttbar.root'     ,'ttbar')
    filetools.filemap('OUT_fatjet_mc/hist-singletop.root' ,'singletop')
    filetools.filemap('OUT_fatjet_data/hist-data.root'    ,'data')

    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309450.PowhegPy8EG_NNLOPS_nnlo_30_ggH125_bb_kt200.fatjet.20180902-01_tree.root.root' ,'ggf')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.345931.PowhegPy8EG_NNPDF30_AZNLOCTEQ6L1_VBFH125_bb.fatjet.20180902-01_tree.root.root','vbf')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309451.Pythia8EvtGen_A14NNPDF23LO_WH125_bb_fj350.fatjet.20180520-01_tree.root.root'  ,'wh')
    filetools.filemap('OUT_fatjet_mc/hist-user.kkrizka.mc16_13TeV.309452.Pythia8EvtGen_A14NNPDF23LO_ZH125_bb_fj350.fatjet.20180520-01_tree.root.root'  ,'zh')
    filetools.filemap('OUT_fatjet_mc/hist-sig.root'                                                                                                    ,'higgs')

    fh_cutflow=open('cutflow.tex','w')

    allmc =['w','z','ttbar','singletop',              'qcd']
    scales=[  1,  1,      1,          1, constants.qcdscale]
    
    generate_table('qcd'      ,constants.selectionCR ,'CRqcdQCD'      ,scale=constants.qcdscale,fhout=fh_cutflow)
    generate_table('w'        ,constants.selectionCR ,'CRqcdW'        ,scale=1.                ,fhout=fh_cutflow)
    generate_table('z'        ,constants.selectionCR ,'CRqcdZ'        ,scale=1.                ,fhout=fh_cutflow)
    generate_table('ttbar'    ,constants.selectionCR ,'CRqcdTTbar'    ,scale=1.                ,fhout=fh_cutflow)
    generate_table('singletop',constants.selectionCR ,'CRqcdSingleTop',scale=1.                ,fhout=fh_cutflow)
    generate_table(allmc      ,constants.selectionCR ,'CRqcdTotal'    ,scale=scales            ,fhout=fh_cutflow)
    generate_table('data'     ,constants.selectionCR ,'CRqcdData'     ,fhout=fh_cutflow)

    generate_table('qcd'      ,constants.selectionVR ,'VRqcdQCD'      ,scale=constants.qcdscale,fhout=fh_cutflow)
    generate_table('w'        ,constants.selectionVR ,'VRqcdW'        ,scale=1.                ,fhout=fh_cutflow)
    generate_table('z'        ,constants.selectionVR ,'VRqcdZ'        ,scale=1.                ,fhout=fh_cutflow)
    generate_table('ttbar'    ,constants.selectionVR ,'VRqcdTTbar'    ,scale=1.                ,fhout=fh_cutflow)
    generate_table('singletop',constants.selectionVR ,'VRqcdSingleTop',scale=1.                ,fhout=fh_cutflow)
    generate_table(allmc      ,constants.selectionVR ,'VRqcdTotal'    ,scale=scales            ,fhout=fh_cutflow)
    generate_table('data'     ,constants.selectionVR ,'VRqcdData'     ,fhout=fh_cutflow)
    
    generate_table('qcd'      ,constants.selectionSR ,'SRQCD'      ,scale=constants.qcdscale,fhout=fh_cutflow)
    generate_table('w'        ,constants.selectionSR ,'SRW'        ,scale=1.                ,fhout=fh_cutflow)
    generate_table('z'        ,constants.selectionSR ,'SRZ'        ,scale=1.                ,fhout=fh_cutflow)
    generate_table('ttbar'    ,constants.selectionSR ,'SRTTbar'    ,scale=1.                ,fhout=fh_cutflow)
    generate_table('singletop',constants.selectionSR ,'SRSingleTop',scale=1.                ,fhout=fh_cutflow)
    generate_table(allmc      ,constants.selectionSR ,'SRTotal'    ,scale=scales            ,fhout=fh_cutflow)
    generate_table('data'     ,constants.selectionSR ,'SRData'     ,fhout=fh_cutflow)

    generate_table('ggf'      ,constants.selectionCR ,'CRqcdGGF'  ,scale=1.                ,fhout=fh_cutflow)
    generate_table('vbf'      ,constants.selectionCR ,'CRqcdVBF'  ,scale=1.                ,fhout=fh_cutflow)
    generate_table('wh'       ,constants.selectionCR ,'CRqcdWH'   ,scale=1.                ,fhout=fh_cutflow)
    generate_table('zh'       ,constants.selectionCR ,'CRqcdZH'   ,scale=1.                ,fhout=fh_cutflow)
    generate_table('higgs'    ,constants.selectionCR ,'CRqcdHiggs',scale=1.                ,fhout=fh_cutflow)

    generate_table('ggf'      ,constants.selectionVR,'VRqcdGGF'  ,scale=1.                ,fhout=fh_cutflow)
    generate_table('vbf'      ,constants.selectionVR,'VRqcdVBF'  ,scale=1.                ,fhout=fh_cutflow)
    generate_table('wh'       ,constants.selectionVR,'VRqcdWH'   ,scale=1.                ,fhout=fh_cutflow)
    generate_table('zh'       ,constants.selectionVR,'VRqcdZH'   ,scale=1.                ,fhout=fh_cutflow)
    generate_table('higgs'    ,constants.selectionVR,'VRqcdHiggs',scale=1.                ,fhout=fh_cutflow)
    
    generate_table('ggf'      ,constants.selectionSR ,'SRGGF'     ,scale=1.                ,fhout=fh_cutflow)
    generate_table('vbf'      ,constants.selectionSR ,'SRVBF'     ,scale=1.                ,fhout=fh_cutflow)
    generate_table('wh'       ,constants.selectionSR ,'SRWH'      ,scale=1.                ,fhout=fh_cutflow)
    generate_table('zh'       ,constants.selectionSR ,'SRZH'      ,scale=1.                ,fhout=fh_cutflow)
    generate_table('higgs'    ,constants.selectionSR ,'SRHiggs'   ,scale=1.                ,fhout=fh_cutflow)

    for Zpmass in constants.Zpmasses:
        filestr=constants.Zpfile(Zpmass)

        generate_table(filestr, constants.selectionCR ,'CRqcd{}'.format(filestr),scale=1.                ,fhout=fh_cutflow)
        generate_table(filestr, constants.selectionVR ,'VRqcd{}'.format(filestr),scale=1.                ,fhout=fh_cutflow)
        generate_table(filestr, constants.selectionSR ,'SR{}'.format(filestr),scale=1.                ,fhout=fh_cutflow)        
    
    fh_cutflow.close()
