from prot import filetools
from prot import utiltools
from prot import textools

import ROOT

from math import *

from hbbisrnote import constants

def calc_soversqrtb(sig,selection,massRange,fh_tex):
    hs=utiltools.Get('{}:/{}/Hcand_m'.format(sig,selection))
    hb=utiltools.Get('bkg:/{}/Hcand_m'.format(selection))

    s=hs.Integral(hs.FindBin(massRange[0]),hs.FindBin(massRange[1]))
    b=hb.Integral(hb.FindBin(massRange[0]),hb.FindBin(massRange[1]))

    sgn=s/sqrt(b)
    
    print(sig,selection,s,b,sgn)
    textools.write_command(fh_tex,'ZprimeSignificance{}{}'.format(sig,selection),sgn,fmt=':0.2f')

def main():
    masses=[100,125,150,175,200,250,300]
    filelist=[]

    for mass in masses:
        infile='hist-mc16_13TeV.MGPy8EG_N30LO_A14N23LO_dmA_jjj_Jet350P100_mRp{}_gSp25.root'.format(str(mass).replace('0',''))
        massstr='mR{}'.format(mass)
        filetools.filemap('OUT_fatjet_mc/{}'.format(infile),massstr)

    filetools.filemap('OUT_fatjet_mc/hist-qcd.root'      ,'bkg')
    filetools.filemap('OUT_fatjet_mc/hist-w.root'        ,'w')
    filetools.filemap('OUT_fatjet_mc/hist-z.root'        ,'z')
    filetools.filemap('OUT_fatjet_mc/hist-v.root'        ,'v')
    filetools.filemap('OUT_fatjet_mc/hist-ttbar.root'    ,'ttbar')
    filetools.filemap('OUT_fatjet_mc/hist-singletop.root','singletop')
    filetools.filemap('OUT_fatjet_mc/hist-sig.root'      ,'sig')

    fh_tex=open('zprimesoversqrtb.tex','w')

    calc_soversqrtb('w', constants.selectionCR, (90-25,90+25), fh_tex=fh_tex)
    calc_soversqrtb('w', constants.selectionVR, (90-25,90+25), fh_tex=fh_tex)
    calc_soversqrtb('w', constants.selectionSR, (90-25,90+25), fh_tex=fh_tex)
    
    calc_soversqrtb('z', constants.selectionCR, (90-25,90+25), fh_tex=fh_tex)
    calc_soversqrtb('z', constants.selectionVR, (90-25,90+25), fh_tex=fh_tex)
    calc_soversqrtb('z', constants.selectionSR, (90-25,90+25), fh_tex=fh_tex)

    calc_soversqrtb('v', constants.selectionCR, (90-25,90+25), fh_tex=fh_tex)
    calc_soversqrtb('v', constants.selectionVR, (90-25,90+25), fh_tex=fh_tex)
    calc_soversqrtb('v', constants.selectionSR, (90-25,90+25), fh_tex=fh_tex)

    calc_soversqrtb('sig', constants.selectionCR, (125-25,125+25), fh_tex=fh_tex)
    calc_soversqrtb('sig', constants.selectionVR, (125-25,125+25), fh_tex=fh_tex)
    calc_soversqrtb('sig', constants.selectionSR, (125-25,125+25), fh_tex=fh_tex)
    
    for mass in masses:
        sig='mR{}'.format(mass)
        calc_soversqrtb(sig, constants.selectionCR, (mass-25,mass+25), fh_tex=fh_tex)
        calc_soversqrtb(sig, constants.selectionVR, (mass-25,mass+25), fh_tex=fh_tex)
        calc_soversqrtb(sig, constants.selectionSR, (mass-25,mass+25), fh_tex=fh_tex)        

    fh_tex.close()
