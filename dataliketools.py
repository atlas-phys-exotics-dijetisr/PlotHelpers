import ROOT
import plotUtils
from math import *

#******************************************
# massLimit = Bin upper edge must be above this limit
def getDataLikeHistsNew( scaledHists, names, jobSeed = 10):
    rand3 = ROOT.TRandom3(1986) #1986 #ORIGINAL

    dataLikeHists = []

    for iHist, scaledHist in enumerate(scaledHists):
        rand3.SetSeed(jobSeed)

        # Create data-like hist
        dataLikeHist=scaledHist.Clone(names[iHist])
        dataLikeHist.SetTitle(names[iHist])
        dataLikeHist.Reset()
        dataLikeHist.SetDirectory(0)

        # Fill it
        events=int(scaledHist.Integral())
        dataLikeHist.FillRandom(scaledHist, rand3.Poisson(events))

        dataLikeHists.append(dataLikeHist)

    return dataLikeHists


def makeDataLikeFromHist(hist,outLumi=3,inLumi=1e-3,seed=10,ignoreNeff=False,nameTag=''):
    outLumis=outLumi if type(outLumi)==list else [outLumi]

    nameTagUS=nameTag+'_' if nameTag!='' else nameTag

    #get effective entries histogram before scaling
    effEntHist = plotUtils.getEffectiveEntriesHistogram(hist, hist.GetName()+'_EffectiveEntries'+nameTag)
    effEntHist.SetTitle('Effective Entries')

    #scale histogram
    scaledHists=[]
    names=[]
    print(outLumis)
    for outLumi in outLumis:
        outLumiStr=('%0.2ffb'%outLumi).replace('.','p')
        scaledHist=hist.Clone(hist.GetName()+'_Scaled_'+nameTagUS+outLumiStr)
        scaledHist.SetTitle('Monte Carlo')
        scaledHist.Scale(float(outLumi)/float(inLumi))
        scaledHists.append(scaledHist)
        names.append(hist.GetName()+'_DataLike_'+nameTagUS+outLumiStr)

    #get data-like histogram
    dataLikeHists = plotUtils.getDataLikeHists(effEntHist, scaledHists, names, jobSeed=seed, massLimit=169, ignoreNeff=ignoreNeff)
    #dataLikeHists = getDataLikeHistsNew(scaledHists, names, jobSeed=seed)
    for i in range(len(dataLikeHists)):
        dataLikeHist=dataLikeHists[i]
        if dataLikeHist==None:
            dataLikeHist=scaledHists[i].Clone()
            dataLikeHist.SetName(names[i])
            dataLikeHist.Reset()
            dataLikeHists[i]=dataLikeHist
        dataLikeHist.SetTitle('Data-Like')

    return effEntHist,scaledHists,dataLikeHists
