import ROOT

from prot import canvastools
from prot import fittools
from prot import filetools

def main(inpath,outpath):
    fh=filetools.open(inpath)
    fh_out=filetools.open(outpath,'RECREATE')

    for key in fh.GetListOfKeys():
        if not key.GetName().startswith('Pt_'): continue
        if key.GetName()!='Pt_offBTag70_bMedium_Total': continue
        print(key.GetName())

        graph=key.ReadObj()
        func=ROOT.TF1(key.GetName(),"[0]*TMath::Landau(x,[1],[2],0)",80,10000);
        func.SetParameter(1,100)
        func.SetParameter(2,200)

        fittools.fit(graph,func,ratiorange=(-0.1,0.1),fitrange=(80,700),xrange=(80,700),fitopt='')
        canvastools.save('plots/trigSF/%s.pdf'%key.GetName())

        fh_out.cd()
        func.Write()

    fh_out.Close()
