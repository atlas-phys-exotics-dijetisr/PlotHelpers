#!/usr/bin/env python

import os, sys, time, copy, re
from array import array
from math import sqrt, log, isnan, isinf

from prot import filetools
from prot import canvastools
from prot import plottools
from prot import utiltools
from prot import style
import dijettools

import ROOT
import DijetFuncs
import Minuit2Fit
import singleFit

def main(infile,sim=True,**kwargs):
    canvastools.savesetup('wilks',**kwargs)

    ### Get input data file ###
    fileIn = filetools.open(infile,'UPDATE')
    if not fileIn:
        print("ERROR, could not find file ", infile)
        exit(1)

    # Process selections
    origsavepath=canvastools.savepath
    dijetdata=dijettools.DijetData(fileIn)
    maxlumi=0
    for selection,lumidatas in dijetdata.selections.items():
        if selection not in ['trijet_j430_2j25','dijetgamma_g150_2j25']: continue
        canvastools.savepath=origsavepath+'/'+selection
        funcs=set()
        g_NLLs={}
        g_Chis={}
        g_prob={}
        g_Wilks={}
        g_allWilks={}
        g_params=[]
        gidxs={}
        for lumi,item in lumidatas.items():
            maxlumi=max(lumi,maxlumi)
            for func in item.fitresultLogL.keys():
                fitresultChi2=item.fitresultChi2[func]
                fitresultLogL=item.fitresultLogL[func]

                funcs.add(func)
                fitInfo=DijetFuncs.getFit(func)

                if func not in gidxs:
                    gidxs[func]=0
                    g_NLLs[func]=ROOT.TGraph()
                    g_NLLs[func].SetTitle(fitInfo.fDisplayName)
                    g_NLLs[func].SetLineColor(fitInfo.fColor)
                    g_NLLs[func].SetMarkerColor(fitInfo.fColor)
                    g_Chis[func]=ROOT.TGraph()
                    g_Chis[func].SetTitle(fitInfo.fDisplayName)
                    g_Chis[func].SetLineColor(fitInfo.fColor)
                    g_Chis[func].SetMarkerColor(fitInfo.fColor)
                    g_prob[func]=ROOT.TGraph()
                    g_prob[func].SetTitle(fitInfo.fDisplayName)
                    g_prob[func].SetLineColor(fitInfo.fColor)
                    g_prob[func].SetMarkerColor(fitInfo.fColor)
                    g_Wilks[func]={}
                    for pIdx in range(len(fitInfo.fParams)):
                        g_param=ROOT.TGraph()
                        g_param.SetTitle(fitInfo.fDisplayName)
                        g_param.SetLineColor(fitInfo.fColor)
                        g_param.SetMarkerColor(fitInfo.fColor)
                        if pIdx>=len(g_params):
                            g_params.append({})
                        g_params[pIdx][func]=g_param

                g_NLLs[func].SetPoint(gidxs[func],lumi,fitresultLogL.MinFcnValue())
                g_Chis[func].SetPoint(gidxs[func],lumi,fitresultChi2.MinFcnValue())
                g_prob[func].SetPoint(gidxs[func],lumi,ROOT.TMath.Prob(fitresultChi2.MinFcnValue(),
                                                                       fitresultChi2.Ndf()))

                for func2 in item.fitresultLogL.keys():
                    if func==func2: continue

                    fitInfo0=DijetFuncs.getFit(func)
                    fitInfo1=DijetFuncs.getFit(func2)
                    ndof=len(fitInfo1.fParams)-len(fitInfo0.fParams)
                    if ndof<0: continue

                    if func2 not in g_Wilks[func]:
                        g_Wilks[func][func2]=ROOT.TGraph()
                        g_Wilks[func][func2].SetTitle(fitInfo1.fDisplayName)
                        g_Wilks[func][func2].SetLineColor(fitInfo1.fColor)
                        g_Wilks[func][func2].SetMarkerColor(fitInfo1.fColor)
                        if ndof==1:
                            g_allWilks[func]=g_Wilks[func][func2].Clone()
                            g_allWilks[func].SetTitle(fitInfo.fDisplayName)
                            g_allWilks[func].SetLineColor(fitInfo0.fColor)
                            g_allWilks[func].SetMarkerColor(fitInfo0.fColor)

                    NLL0=item.fitresultLogL[func] .MinFcnValue()
                    NLL1=item.fitresultLogL[func2].MinFcnValue()

                    g_Wilks[func][func2].SetPoint(gidxs[func],lumi,
                                                  ROOT.TMath.Prob(max(2*(NLL0-NLL1),0),
                                                                  ndof))
                    if ndof==1:
                        g_allWilks[func].SetPoint(gidxs[func],lumi,
                                                  ROOT.TMath.Prob(max(2*(NLL0-NLL1),0),
                                                                  ndof))

                for pIdx in range(len(fitInfo.fParams)):
                    g_params[pIdx][func].SetPoint(gidxs[func], lumi, fitresultLogL.Parameter(pIdx))

                gidxs[func]+=1

        if len(gidxs.keys())==0: continue

        # Plot
        l5=ROOT.TLine(0,0.05,maxlumi,0.05)
        l5.SetLineStyle(ROOT.kDashed)
        
        plottools.graphs(sorted(g_NLLs.values(),key=lambda g: g.GetTitle()),
                         xtitle='Luminosity (fb^{-1})',
                         ytitle='-Log(L)',
                         xrange=(0,maxlumi),
                         opt='PL',
                         logy=True,
                         legend=(0.6,0.5),
                         textpos=(0.5,0.6),
                         sim=sim,
                         text=selection)
        canvastools.save('NLL.svg')

        plottools.graphs(g_Chis.values(),
                         xtitle='Luminosity (fb^{-1})',
                         ytitle='#chi^{2}',
                         xrange=(0,maxlumi),
                         opt='PL',
                         legend=(0.6,0.5),
                         textpos=(0.5,0.6),
                         sim=sim,
                         text=selection)
        canvastools.save('Chi2.svg')

        plottools.graphs(sorted(g_prob.values(),key=lambda g: g.GetTitle()),
                         xtitle='Luminosity (fb^{-1})',
                         ytitle='P(#chi^{2})',
                         xrange=(0,maxlumi),
                         opt='PL',
                         yrange=(0,1),
                         legend=(0.6,0.5),
                         textpos=(0.5,0.6),
                         sim=sim,
                         text=selection)
        canvastools.save('prob.svg')

        plottools.graphs(sorted(g_allWilks.values(),key=lambda g: g.GetTitle()),
                         xtitle='Luminosity (fb^{-1})',
                         ytitle='Wilks\' Statistic',
                         xrange=(0,maxlumi),
                         yrange=(1e-3,1.),
                         opt='PL',
                         logy=True,
                         legend=(0.7,0.37),
                         textpos=(0.5,0.45),
                         sim=sim,
                         text=selection)
        canvastools.canvas().cd()
        l5.Draw()
        canvastools.save('wilks.svg')

        for func,g_Wilk in g_Wilks.items():
            if len(g_Wilk.values())==0: continue
            fitInfo=DijetFuncs.getFit(func)
            plottools.graphs(g_Wilk.values(),
                             xtitle='Luminosity (fb^{-1})',
                             ytitle='Wilks\' Statistic',
                             xrange=(0,maxlumi),
                             yrange=(1e-3,1.),
                             opt='PL',
                             logy=True,
                             legend=(0.6,0.5),
                             textpos=(0.5,0.6),
                             sim=sim,
                             text=selection+'\n'+fitInfo.fDisplayName+' as NULL')
            l5.Draw()            
            canvastools.save('wilks-%s.svg'%(func))

        paramranges=[(10,2e5),
                     (40,2e2),
                     (1e-4,2),
                     (1e-2,1e-1),
                     (1e-5,1e-1),
                     (1e-5,1e-1)]
        for pIdx in range(len(g_params)):
            plottools.graphs(sorted(g_params[pIdx].values(), key=lambda g: g.GetTitle()),
                             xtitle='Luminosity (fb^{-1})',
                             ytitle='Parameter %d'%pIdx,
                             xrange=(0,maxlumi),
                             opt='PL',
                             logy=True,
                             #yrange=paramranges[pIdx],
                             legend=(0.6,0.45),
                             textpos=(0.2,0.25),
                             sim=sim,
                             text=selection)
            canvastools.save('param-%d.svg'%(pIdx))

 
    fileIn.Close()

    canvastools.savereset()
