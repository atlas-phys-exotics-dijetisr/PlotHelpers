import ROOT

from prot import filetools

import csv
import glob
import os, os.path

import dijettools

def main(dataDir,lumicsv='data/data_HLT_j380.csv'):
    runs={}
    # Find all runs
    with open(lumicsv) as fhlumi:
        reader = csv.DictReader(fhlumi)
        for row in reader:
            runs[int(row['Run'])]=float(row['LAr Corrected'])/1000.

    # Prepare output
    fh_out=filetools.open(dataDir+'/datalike.root','RECREATE')
    filetools.filemap(dataDir+'/datalike.root','output')

    # Loop
    totallumi=0
    lumisincesave=0
    inHists=sorted(glob.glob(dataDir+'/hist-*.root'))
    first=True
    lastHists={}    
    for inHist in inHists:
        # Lumi update
        parts=os.path.basename(inHist).split('.')
        run=int(parts[3])
        lumi=runs.get(run,0)
        totallumi+=lumi
        lumisincesave+=lumi

        # Selection loop
        saved=False
        fh_in=filetools.open(inHist)
        for key in fh_in.GetListOfKeys():
            selection=key.GetName()
            obj=key.ReadObj()
            if not obj.InheritsFrom(ROOT.TDirectoryFile.Class()): continue
            seldir=obj
            if first: # Make everything new!
                fh_out.mkdir(selection).cd()
                lastHist=seldir.Get('Zprime_mjj_var').Clone('Zprime_mjj_var_DataLike_0p00fb')
                lastHist.SetTitle('Data')
                lastHist.Reset()
                lastHists[selection]=lastHist

            # Add this block
            addHist=key.ReadObj().Get('Zprime_mjj_var')
            lastHists[selection].Add(addHist)

            # Save
            if lumisincesave>0.01:
                fh_out.cd(selection)
                lastHists[selection].SetName('Zprime_mjj_var_DataLike_%sfb'%dijettools.lumistr(totallumi))
                lastHists[selection].Write()
                saved=True

        if saved:
            lumisincesave=0.

        first=False

    #fh_out.Close()
