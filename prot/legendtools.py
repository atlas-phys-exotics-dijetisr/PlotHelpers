import ROOT

from prot import utiltools

def make(c1):
    l=ROOT.TLegend(0,0,0,0)
    l.SetBorderSize(0)
    l.SetFillStyle(0)
    l.SetTextFont(l.GetTextFont()-l.GetTextFont()%10+3)
    utiltools.store.append(l)
    return l

def add(l,objs):
    next=ROOT.TIter(objs)
    for obj in next:
        # Composite objects
        if obj.InheritsFrom(ROOT.THStack.Class()):
            add(l,obj.GetHists())
            continue
        elif obj.InheritsFrom(ROOT.TMultiGraph.Class()):
            add(l,obj.GetListOfGraphs())
            continue


        # Objects we want to skip
        if not (obj.InheritsFrom(ROOT.TAttLine.Class()) or
                obj.InheritsFrom(ROOT.TAttFill.Class()) or
                obj.InheritsFrom(ROOT.TAttMarker.Class())):
            continue

        # Specific objects we want to skip
        if obj.InheritsFrom(ROOT.TFrame.Class()) or obj.InheritsFrom(ROOT.TPave.Class()):
            continue

        # Something we want to add
        opts=""
        gOpt=next.GetOption().lower()

        if obj.InheritsFrom(ROOT.TH1.Class()):
            if 'hist' in gOpt:
                if obj.GetFillColor()==0: opts="L"
                else: opts="F"
            else: opts="LP"
        elif obj.InheritsFrom(ROOT.TGraph.Class()):
            if '2' in gOpt or '3' in gOpt: opts='F'
            elif gOpt!='':
                if 'c' in gOpt or 'l' in gOpt: opts+='L'
                if 'p' in gOpt: opts+='P'
                print(opts)
            else: opts='LPF'
        elif obj.InheritsFrom(ROOT.TF1.Class()):
            opts='L'
        else:
            if obj.InheritsFrom(ROOT.TAttLine.Class()): opts+="L"
            if obj.InheritsFrom(ROOT.TAttFill.Class()): opts+="F"
            if obj.InheritsFrom(ROOT.TAttMarker.Class()): opts+="P";

        if obj.GetTitle()!='':
            l.AddEntry(obj,obj.GetTitle(),opts)

def resize(l,pos=None,bottomup=False,width=0.4):
    if pos==None:
        pos=(l.GetX1(),l.GetY2())

    x1,y2,width,height=pos[0],pos[1],width,0.06

    if bottomup:
        l.SetY2(y2+height*l.GetNRows())
        l.SetY1(y2)
    else:
        l.SetY1(y2-height*l.GetNRows())
        l.SetY2(y2)
    l.SetX1(x1)
    l.SetX2(min(x1+width*l.GetNColumns(),0.9))

def reorder(l,titles):
    entries=l.GetListOfPrimitives()
    goalidx=0
    for testidx in range(len(titles)):
        found=False
        for foundidx in range(entries.GetEntries()):
            if entries.At(foundidx).GetLabel()==titles[testidx]:
                found=True
                break
        if not found: continue # not found, move on to the next one
        if foundidx!=goalidx:
            entry=entries.At(foundidx)
            entries.Remove(entry)
            entries.AddAt(entry,goalidx)
        goalidx+=1

