import ROOT

from prot import filetools

def Get(name):
    if type(name)!=str: return name
    
    path=None
    fh=None
    if ':/' in name:
        parts=name.split(':/')
        fname=parts[0]
        path=':/'.join(parts[1:])

        if fname=='_':
            fh=ROOT.gROOT
        else:
            fh=filetools.fm.open(fname)
    else:
        path=name
        fh=ROOT.gDirectory

    if path=='': return fh
    else: return fh.Get(path)


def filetag(f):
    if type(f)!=tuple:
        return (f,{})
    return f

def dictsort(d):
    items=d.items()
    items=sorted(items,key=lambda item: item[0])
    key,val=zip(zip(*items))
    return list(val[0])

store=[]
