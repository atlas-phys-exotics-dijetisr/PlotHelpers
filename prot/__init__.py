import ROOT
import glob
import os

#
# Load useful colours
colorfiles=[]
colorfiles+=glob.glob('colors/*.color')
colorfiles+=glob.glob(os.path.dirname(os.path.realpath(__file__))+'/../colors/*.color')
for colorfile in colorfiles:
    fh=open(colorfile)
    for line in fh:
        line=line.strip()
        if line=='': continue
        parts=line.split()
        name=parts[0].replace('color.','')
        code=parts[1]
        colorIdx=ROOT.TColor.GetColor(code)
        setattr(ROOT,name,colorIdx)
