import ROOT

from prot import utiltools
from prot import filetools
from prot import plottools
from prot import canvastools
from prot import prettytools
from prot import histtools
from prot import style

import selectiontools

import re
import glob
import shutil
import copy
import datetime
import os, os.path

def plotCompare(hs,histname,**kwargs):
    # style requests
    stylename=histname.split('/')[-1]    

    #if 'etajj' not in histname: return

    if stylename in style.style.data: kwargs.update(style.style.data[stylename])

    #
    # Settings
    normalize=kwargs.get('normalize',False)
    logy=kwargs.get('logy',False)
    dijetard=kwargs.get('dijetard',False)
    xrange=style.torange(kwargs.get('xrange',(None,None)))
    h0=hs[0]

    #
    # Automatic pretty

    # Build up the profile of the nostack histogram
    hmaxrange=h0[0].Clone()
    hmaxrange.Reset()

    hminrange=h0[0].Clone()
    hminrange.Reset()

    integrals=[h[0].Integral() if h[0].Integral()>0 else 1. for h in hs]
    scales=[hs[idx][1].get('scale',1) if not normalize else 1./integrals[idx] for idx in range(len(hs))]

    for binIdx in range(0,hmaxrange.GetNbinsX()+2):
        vals=[hs[idx][0].GetBinContent(binIdx)*scales[idx] for idx in range(len(hs))]
        hmaxrange.SetBinContent(binIdx,max(vals))
        hminrange.SetBinContent(binIdx,min(vals))

    # Graph type
    direction='<'
    minBin,maxBin=histtools.binrange(hmaxrange,*xrange)
    minBinAb0=max(minBin,hmaxrange.FindFirstBinAbove(0))
    maxBinAb0=min(maxBin,hmaxrange.FindLastBinAbove (0))

    occCentering=hmaxrange.Integral(minBin   ,int((minBin   +maxBin)/2)) / max(hmaxrange.Integral(int((minBin   +maxBin)/2),maxBin),1e-5)
    occupancy='<'    
    if occCentering>1:
        occupancy='>'
    if abs(1-occCentering)<0.3:
        occupancy='='

    dirCentering=hmaxrange.Integral(minBinAb0,int((minBinAb0+maxBinAb0)/2)) / max(hmaxrange.Integral(int((minBinAb0+maxBinAb0)/2),maxBinAb0),1e-5)
    if dirCentering>1:
        direction='>'
    if abs(1-dirCentering)<0.3:
        direction='='

    # Positions
    textpos=(0.6,0.8)
    legend=(0.17,0.93)
    if occupancy=='>':
        textpos=(0.6,0.8)
        legend=(0.6,0.75)
    elif occupancy=='=':
        textpos=(0.6,0.8)
        legend=(0.17,0.93)
    else:
        textpos=(0.17,0.8)
        legend=(0.17,0.75)

    #
    # Determine yrange by looking at background
    scale=kwargs.get('rebin',1)
    if direction=='=': scale*=1.25
    if direction=='>' and logy and occCentering<2.5: scale*=1.75
    padratio=0.5 if logy else 0.1

    # the actual range
    if logy:
        yrange=(prettytools.getminimum(hminrange,*xrange,direction=direction)[1]*(1-padratio)*scale,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)
    else:
        yrange=(0,
                prettytools.getmaximum(hmaxrange,*xrange)[1]*(1+padratio)*scale)

    print('direction',dirCentering,direction,yrange,1+padratio,scale)
    print('occupancy',occCentering,occupancy,yrange,1+padratio,scale)    

    #
    # Determine rebinning based on data
    newbins=None
    if logy and stylename!='Zprime_mjj_var':
        newbins=[]
        rebinNow=1
        rebinIdx=0
        maxdatabin,maxdata=histtools.getmaximum(hmaxrange)
        for binIdx in range(1,hmaxrange.GetNbinsX()+2):
            if rebinIdx==0:
                newbins.append(hmaxrange.GetBinLowEdge(binIdx))

                # Determine new binning
                nowdata=hmaxrange.GetBinContent(binIdx)
                rebinNow=min(int(maxdata/nowdata/10)+1 if nowdata>0 else rebinNow,4)
                rebinNow=rebinNow if binIdx>maxdatabin else 1
                rebinIdx=rebinNow-1
            else:
                rebinIdx-=1
        kwargs['rebin']=newbins

    # plot
    plottools.plots(hs,
                    hsopt='nostack',
                    ratio=0,yrange=yrange,legend=legend,textpos=textpos,**kwargs)
    canvastools.save('%s.pdf'%os.path.basename(histname))

def compare(ds,path='',**kwargs):
    ds=[utiltools.filetag(d) for d in ds]
    d0=ds[0]

    oldsavepath=canvastools.savepath
    for key in d0[0].GetListOfKeys():
        name=key.GetName()
        print(path)
        newpath=path+'/'+name
        obj=key.ReadObj()

        allobjs=[(d[0].Get(name),d[1]) for d in ds]
        
        if obj.InheritsFrom(ROOT.TH1F.Class()):
            canvastools.savepath=oldsavepath
            plotCompare(allobjs,newpath,**kwargs)
        elif obj.InheritsFrom(ROOT.TDirectoryFile.Class()):
            if obj.GetName().startswith('sys'): continue
            canvastools.savepath=oldsavepath+'/'+obj.GetName()            
            compare(allobjs,newpath,**kwargs)
    canvastools.savepath=oldsavepath

def study_selection(d,**kwargs):
    # start plotting
    oldsaveextra=canvastools.saveextra

    re_mass=re.compile('hist-mg_Zpj_mR([0-9]+)\.root')
    filename=os.path.basename(d[0].GetFile().GetPath()[0:-2])
    match=re_mass.match(filename)
    mR=int(match.group(1))

    # jet info
    for i in range(3):
        reso=d[0].Get('jet%d_reso'%i)
        isr =d[0].Get('jet%d_isr'%i)
        canvastools.saveextra='%s/jet%d/'%(oldsaveextra,i)
        compare([(reso,{'title':'Resonance'}),(isr,{'title':'ISR'})],normalize=True,**kwargs)

    # correct vs incorrect
    binRange=(mR-50,mR+50)
    corr=d[0].Get('pick_m23/bin%dto%d/correct'%binRange)
    inco=d[0].Get('pick_m23/bin%dto%d/incorrect'%binRange)
    canvastools.saveextra=oldsaveextra
    compare([(corr,{'title':'Correct'}),(inco,{'title':'Incorrect'})],normalize=True,**kwargs)

    # canvastools.saveextra='comb/'
    # compare([(d[0].Get('mjj'),{'title':'Correct'}),
    #          (d[0].Get('m12_isr'),{'title':'m_{12} Incorrect','color':ROOT.kRed}),
    #          (d[0].Get('m13_isr'),{'title':'m_{13} Incorrect','color':ROOT.kBlue}),
    #          (d[0].Get('m23_isr'),{'title':'m_{23} Incorrect','color':ROOT.kGreen+2})],
    #         normalize=True,opt='hist',ratiorange=(0,2),**kwargs)

    canvastools.saveextra=''

def study_fractions(*args,**kwargs):
    re_mass=re.compile('hist-mg_Zpj_mR([0-9]+)\.root')

    print(args)
    gfracs={}
    gidxs ={}
    gidx=0

    mjjplots={}
    for d in args:
        filename=os.path.basename(d[0].GetFile().GetPath()[0:-2])
        match=re_mass.match(filename)
        mR=int(match.group(1))

        # Determine possible picks
        for key in d[0].GetListOfKeys():
            if not key.GetName().startswith('pick_'): continue
            pickMode=key.GetName()[5:]

            if pickMode not in gfracs:
                gfracs[pickMode]=ROOT.TGraph()
                gidxs [pickMode]=0
                mjjplots[pickMode]=[]

            obj=key.ReadObj()

            # mjj plots
            if mR<1000:
                mjjplots[pickMode].append((obj.Get('mjj/mjj'),{'title':'m_{R} = %d'%mR,'color':style.palette[gidx],'opt':'hist'}))

            # Fractions
            corr=obj.Get('mjj_correct/mjj').Integral()
            tota=obj.Get('mjj/mjj')        .Integral()
            frac=corr/tota
            gfracs[pickMode].SetPoint(gidxs[pickMode],mR,frac)
            gidxs[pickMode]+=1
            print(mR,pickMode,frac)

        # incr
        gidx+=1

    plottools.graphs([(gfracs['m12'],{'title':'m_{12}'}),
                      (gfracs['m13'],{'title':'m_{13}','color':ROOT.kRed}),
                      (gfracs['m23'],{'title':'m_{23}','color':ROOT.kBlue})]
                     ,xtitle='m_{R} [GeV]',ytitle='Fraction Correct',opt='C',legend=(0.4,0.9))
    canvastools.save('fraccorrect.pdf')

    #
    # plot picks
    graphs=[]
    for key in sorted(gfracs.keys()):
        value=gfracs[key]
        style.style.apply_style(value,{},key)
        graphs.append(value)
    plottools.graphs(graphs,xtitle='m_{R} [GeV]',ytitle='Fraction Correct',opt='*C',legend=(0.4,0.9))
    canvastools.save('fraccorrectall.pdf')

    #
    # plot peaks
    for key,mjjplot in mjjplots.items():
        mjjplot=sorted(mjjplot,key=lambda x: x[1]['title'])
        plottools.plots(mjjplot,normalize=True,legend=(0.6,0.9),hsopt='nostack',xrange=(0,1000))
        canvastools.save('mjj_%s.pdf'%key)
    
def main(outpath,**kwargs):
    canvastools.savesetup('mjjChoice',**kwargs)


    re_mass=re.compile('hist-mg_Zpj_mR([0-9]+)\.root')    
    files=glob.glob('%s/hist-mg_Zpj_*.root'%outpath)
    for path in files:
        print(path)
        match=re_mass.match(os.path.basename(path))
        mR=int(match.group(1))
        canvastools.saveextra='mR%d'%mR
        selectiontools.loop(study_selection,path,text='m_{R} = %d GeV'%mR,**kwargs)
    canvastools.saveextra=''

    selectiontools.loop(study_fractions,*files,**kwargs)
        
    canvastools.savereset()

