import glob
import os, os.path

import plot_datamc
import makeAcceptanceTable
import plot_xseccomp
import cutStudy
import makeDataLike
import addDataHists
import runSingleFit
import makeSysInputs
import plotSingleFit
import plotFitInfo
import quickLimits
import table_cutflow
import table_acceff
import plot_systematics
import plot_accuncert
import plot_accuncert_jesscenarios
import plot_acceptances

from prot import style
from prot import utiltools

import zprimetools

import ROOT

def runCutStudy():
    #
    # Cut optimizations
    style.style.parse('styles/cutvariables.style')
    cutStudy.main("OUT_dijetgamma_mc/hist.root",
                  [("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.305161.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp3_mD10_gSp3_gD1.gammajet.20160726-01_tree.root.root",{'title':'m_{R}=300 GeV','linecolor':ROOT.kBlue}),
                   ("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.305163.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp5_mD10_gSp3_gD1.gammajet.20160726-01_tree.root.root",{'title':'m_{R}=500 GeV','linecolor':ROOT.kRed})],
                   lumi=15.45e3)


def runDataMC():
    #
    # Data vs MC comparisons
    oldstyledata=style.style.data
    style.style.parse('styles/variables.style')
    plot_datamc.main(("OUT_dijetgamma_mc/hist.root",{"title":"Sherpa #gamma+jet"}),
                     ("OUT_dijetgamma_data/hist.root",{"title":"Data"}),
                     ("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.305161.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp3_mD10_gSp3_gD1.gammajet.20160726-01_tree.root.root",{"title":"m_{R}=300 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),
                     #("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.305163.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp5_mD10_gSp3_gD1.gammajet.20160726-01_tree.root.root",{"title":"m_{R}=500 GeV, g_{SM}=0.3 (x100)","scale":100,"linecolor":ROOT.kBlue}),
                     ratiorange=(0.5,1.5),ytitle="Events",text='',lumi=15.45,sim=False,selection='dijetgamma_g150_2j25*')
    style.style.data=oldstyledata

def runSystematicPlots():
    oldstyledata=style.style.data
    style.style.parse('styles/variables.style')

    signals=glob.glob("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_*gSp3*.gammajet.*_tree.root.root")

    newsignals=[]
    for signal in signals:
        mR,mDM,gSM,gDM=zprimetools.info(signal)
        nameTag=zprimetools.nameTag(signal)
        plot_systematics.main(signal,nameTag=nameTag,mjjrange=(mR-100,mR+100))

    style.style.data=oldstyledata        

def runCutflow():
    table_cutflow.main("OUT_dijetgamma_data/hist.root",cfname='dijetgamma_data')
    table_cutflow.main("OUT_dijetgamma_mc/hist.root"  ,cfname='dijetgamma_mc' ,scale=15.45e3*1.52)

    # signals
    table_cutflow.main("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.305161.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp3_mD10_gSp3_gD1.gammajet.20160726-01_tree.root.root",cfname='dijetgamma_sigThreeHundred', scale=15.45e3)
    table_cutflow.main("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.305163.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_mRp5_mD10_gSp3_gD1.gammajet.20160726-01_tree.root.root",cfname='dijetgamma_sigFiveHundred',  scale=15.45e3)

def runAcceptance():
    makeAcceptanceTable.main('OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_*','OUT_dijetgamma_mc/signal_acceptances.root',truthpath='OUT_dijetgamma_truth')
    plot_acceptances.main('OUT_dijetgamma_mc/signal_acceptances.root')
    table_acceff.main('OUT_dijetgamma_mc/signal_acceptances.root',aename='dijetgamma')

def runDatalike():
    outLumis=[0.1,0.3,0.5,0.7,0.9,1.,3.,3.21,5.,7.,9.,10.,12.,12.24,14.,15.45,16.,18.,20.]

    makeDataLike.main('OUT_dijetgamma_mc/hist.root'                            ,outLumis=outLumis)
    makeDataLike.main('OUT_dijetgamma_mc/hist-*.mc15_13TeV.*SinglePhoton*.root',outLumis=outLumis,split='hist-split.root')
    
    makeDataLike.main('OUT_dijetgamma_mc/hist.root'                            ,outLumis=outLumis,                        outName='datalike-noNeff',ignoreNeff=True)
    makeDataLike.main('OUT_dijetgamma_mc/hist-*.mc15_13TeV.*SinglePhoton*.root',outLumis=outLumis,split='hist-split.root',outName='datalike-noNeff',ignoreNeff=True)

    signals=glob.glob('OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_*.dijet.*_tree.root.root')       
    for signal in signals:
        newname=zprimetools.nameTag(os.path.basename(signal))
        makeDataLike.main(signal,nameTag=newname,outLumis=outLumis)

    addDataHists.main("OUT_dijetgamma_data")

def runAccUncertainty():
    style.style.parse('styles/systematics.style')
    style.style.parse('styles/jesscenarios.style')

    allsignals=[]
    for scenario in range(1,2):
        signals=glob.glob("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_*gSp3*.gammajet.*-0%d_tree.root.root"%scenario)

        newsignals=[]
        for signal in signals:
            mR,mDM,gSM,gDM=zprimetools.info(signal)
            if mR<250 or 1500<=mR: continue
            allsignals.append((signal,{'mR':mR,'scenario':scenario}))
            newsignals.append((signal,{'mR':mR}))

        if scenario==1:
            plot_accuncert.main(allsignals,textpos=(0.6,0.8),text='')
    plot_accuncert_jesscenarios.main(allsignals,textpos=(0.6,0.8),text='')

def prepSysFiles():
    signals=glob.glob("OUT_dijetgamma_mc/hist-user.kkrizka.mc15_13TeV.*.MGPy8EG_N30LO_A14N23LO_dmA_jja_Ph100_*.gammajet.*_tree.root.root")
    for signal in signals:
        newname=zprimetools.nameTag(os.path.basename(signal))
        makeSysInputs.main(signal,histname='Zprime_mjj_var',nameTag=newname)

def runFits():
    runSingleFit.main("OUT_dijetgamma_mc/datalike/hist.root",massRanges=(169,1493),outFile='OUT_dijetgamma_mc/datalike/fit.root')
    plotSingleFit.main("OUT_dijetgamma_mc/datalike/fit.root",outDir='mc',dijetard=True,massRanges=(169,1493))
    plotFitInfo.main("OUT_dijetgamma_mc/datalike/fit.root",outDir='mc')

    runSingleFit.main("OUT_dijetgamma_mc/datalike/hist-split.root",massRanges=(169,1493),outFile='OUT_dijetgamma_mc/datalike/fit-split.root')
    plotSingleFit.main("OUT_dijetgamma_mc/datalike/fit-split.root",outName='mc_split',dijetard=True,massRanges=(169,1493))
    plotFitInfo.main("OUT_dijetgamma_mc/datalike/fit-split.root",outName='mc_split')

    runSingleFit.main("OUT_dijetgamma_mc/datalike-noNeff/hist.root",massRanges=(169,1493),outFile='OUT_dijetgamma_mc/datalike-noNeff/fit.root')
    plotSingleFit.main("OUT_dijetgamma_mc/datalike-noNeff/fit.root",outName='mc_noNeff',dijetard=True,massRanges=(169,1493))
    plotFitInfo.main("OUT_dijetgamma_mc/datalike-noNeff/fit.root",outName='mc_noNeff')

    runSingleFit.main("OUT_dijetgamma_data/datalike.root",outFile='OUT_dijetgamma_data/fit.root',massRanges=(169,1493))
    plotSingleFit.main("OUT_dijetgamma_data/fit.root",outName='data',dijetard=True,massRanges=(169,1493),sim=False)
    plotFitInfo.main("OUT_dijetgamma_data/fit.root",outName='data',sim=False)

def run2DLimits():
    quickLimits.main("data/limits_g150_2j25.txt"   ,lumi=15.45,         outfile='dijetgamma_g150_2j25/Zprime2D_data.pdf',selection="dijetgamma_g150_2j25")
    quickLimits.main("data/limits_g150_2j25_mc.txt",lumi=15.45,sim=True,outfile='dijetgamma_g150_2j25/Zprime2D_mc.pdf'  ,selection="dijetgamma_g150_2j25")

def main(doCutStudy=False,doDataMC=False,doCutflow=False,doAcceptance=False,doDatalike=False,doFits=False,doAccUncertainty=False,doPrepSysFiles=False,doSystematicPlots=False,do2DLimits=False):
    #
    # Do opitmization study
    if doCutStudy:
        runCutStudy()
    
    #
    # Do data vs MC comparisons
    if doDataMC:
        runDataMC()

    #
    # Make cutflow table
    if doCutflow:
        runCutflow()

    #
    # Make plot with acceptances
    if doAcceptance:
        runAcceptance()
        
    

    #
    # Make datalike statistics
    if doDatalike:
        runDatalike()

    #
    # Run fit tests
    if doFits:
        runFits()

    #
    # Calculate simple acceptance uncertainties
    if doAccUncertainty:
        runAccUncertainty()

    #
    # Prepare systematic files
    if doPrepSysFiles:
        prepSysFiles()

    #
    # Do systematic comparisons
    if doSystematicPlots:
        runSystematicPlots()

    #
    # Run gSM limits
    if do2DLimits:
        run2DLimits()
