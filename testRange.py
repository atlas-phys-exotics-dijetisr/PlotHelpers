import ROOT

from prot import style
from prot import filetools
from prot import fittools
from prot import plottools
from prot import canvastools

import selectiontools

import DijetFuncs
import dijettools

from math import *

def fitRange(d,fitFunc='dijet4par',massRange=(100,1500),step=10,lumi=3.2,sim=True,pseudodata=False,outDir=None,**kwargs):
    outDir='' if outDir==None else outDir+'/'

    random=ROOT.TRandom3(1)
    
    #hist2d=d[0].Get('Debug_h_mjjvsasymISR2').Clone()
    #hist=hist2d.ProjectionY('_py',hist2d.GetXaxis().FindBin(0),hist2d.GetXaxis().FindBin(1.))
    # orighist=hist2d.ProjectionY('orig_py')
    # plottools.plots([(orighist,{'title':'Baseline'}),(hist,{'title':'(#vec{p}_{T}^{1}-#vec{p}_{T}^{2})#upoint#hat{p}_{T}^{jj}/p_{T}^{jj} < 0.9'})],xrange=(0,600),ytitle='#sigma [pb]',ratiorange=(0.5,1.),logy=True,ratiotitle='Ratio',yrange=(5e-1,1e2),legend=(0.6,0.8))
    # canvastools.save('orig.pdf')

    hist=d[0].Get('Zprime_mjj').Clone()

    if sim:
        hist.Scale(lumi*1e3)

        # Fix errors
        for binIdx in range(hist.GetNbinsX()+2):
            val=hist.GetBinContent(binIdx)
            if pseudodata:
                newval=random.Poisson(val)
                hist.SetBinContent(binIdx,newval)
                hist.SetBinError  (binIdx,sqrt(newval))

    # Plotting
    c1=canvastools.canvas()
    hi=massRange[1]

    gidx=0
    g_prob=ROOT.TGraph()
    for lo in range(hi-step,massRange[0]-step,-step):
        fitResult=fittools.fit(hist,fitFunc,fitrange=(lo,massRange[1]),ratiomode='significance',ratiorange=(-5,5),logy=True,fitopt='i',ytitle='Events')
        canvastools.save('%stestFit-%s-%s-mR%dto%d.svg'%(outDir,fitFunc,dijettools.lumistr(lumi),lo,massRange[1]))
        if fitResult==None: continue
        prob=fitResult.Prob()
        g_prob.SetPoint(gidx,lo,prob)
        gidx+=1

    return g_prob

def fitRangeAllFunc(d,massRange=(100,1500),step=10,lumi=3.2,sim=True,pseudodata=False,outDir=None,**kwargs):
    g_funcs=[]
    for fitFunc in ['dijet3par','dijet4par','dijet5par']:
        g_func=fitRange(d,fitFunc=fitFunc,massRange=massRange,step=step,lumi=lumi,sim=sim,pseudodata=pseudodata,outDir=outDir,**kwargs)
        style.style.apply_style(g_func,{},fitFunc)
        g_funcs.append(g_func)

    legend=(0.7,0.4) if sim else (0.5,0.4)

    plottools.graphs(g_funcs,text='%s'%(d[0].GetName()),
                     lumi=lumi,
                     sim=sim,
                     opt='PL',
                     xtitle='Lower Bound [GeV]',
                     ytitle='P(#chi^{2})',
                     legend=legend,
                     textpos=(0.2,0.25))

    outDir='' if outDir==None else outDir+'/'
    canvastools.save('%stestFit-%s.svg'%(outDir,dijettools.lumistr(lumi)))

def main(inpath,**kwargs):
    canvastools.savesetup('testRange',**kwargs)
    selectiontools.loop(fitRangeAllFunc,inpath,**kwargs)
    canvastools.savereset()
