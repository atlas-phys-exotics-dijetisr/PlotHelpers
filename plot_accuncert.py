from prot import systools
from prot import plottools
from prot import style
from prot import canvastools

import zprimetools
import selectiontools

import ROOT

from math import *

def plotAccUncert(*dirs,**kwargs):
    jesnames=['JET_EtaIntercalibration_NonClosure','JET_GroupedNP_1','JET_GroupedNP_2','JET_GroupedNP_3']

    g_sysvsmRs={}
    for dir in dirs:
        mR      =dir[1].get('mR'      ,0)

        hist_nom=dir[0].Get('Zprime_mjj')
        systs=systools.get_syslist(hist_nom,sys=True)

        #
        # Bin by bin
        gsysts={}
        c=0
        for sysname in systs:
            syst=systs[sysname]
            gsyst=systools.apply_systematics(hist_nom,[syst],symmetric=True)
            gsyst.SetTitle(sysname)
            n=gsyst.GetN()
            xs=gsyst.GetX()
            ys=gsyst.GetY()
            for i in range(gsyst.GetN()):
                gsyst.SetPoint(i,xs[i],0)
            gsysts[sysname]=gsyst
            gsyst.SetFillColor(style.palette[c])
            gsyst.SetMarkerColor(style.palette[c])
            gsyst.SetFillColor(style.palette[c])
            c+=1

        # Add JES systematics
        gjessys=ROOT.TGraph()
        gjessys.SetTitle('JET_JES_ALL')


        jesxs=[]
        jesys=[]
        
        for jesname in jesnames:
            gonejessys=gsysts[jesname]

            n=gonejessys.GetN()
            xs=gonejessys.GetX()
            ys=gonejessys.GetY()

            for gidx in range(n):
                if len(jesxs)<=gidx:
                    jesxs.append(xs[gidx])
                    jesys.append(0)
                jesys[gidx]+ys[gidx]**2
            del gsysts[jesname]

        n=gjessys.GetN()
        for gidx in range(n):
            gjessys.SetPoint(gidx,jesxs[gidx],sqrt(jesys[gidx]))

        gsysts['JET_JES_ALL_%d']=gjessys
            
        plottools.graphs(gsysts.values(),xrange=(0,500),opt='2',legend=(0.2,0.6))
        canvastools.save('sys_%d.pdf'%mR)

        # Acceptance uncertainty
        binMin=hist_nom.FindBin(mR-50)
        binMax=hist_nom.FindBin(mR+50)
        events_nom=hist_nom.Integral(binMin,binMax)

        thesysts={}
        for sysname in systs:
            syst=systs[sysname]
            events_systs=[s.Integral(binMin,binMax) for s in syst]
            thesys=max([abs((events_syst-events_nom)/events_nom) for events_syst in events_systs])
            thesysts[sysname]=thesys

        #
        # Add JES systematics
        jessys=0
        for jesname in jesnames:
            jessys+=thesysts.get(jesname,0)**2
            del thesysts[jesname]
        jessys=sqrt(jessys)
        thesysts['JET_JES_ALL']=jessys

        # save to the histogram
        for sysname,thesys in thesysts.items():
            if sysname not in g_sysvsmRs:
                g_sysvsmR=ROOT.TGraph()
                g_sysvsmR.SetTitle(sysname)
                style.style.apply_style(g_sysvsmR,{},sysname)
                g_sysvsmRs[sysname]=g_sysvsmR
            g_sysvsmR=g_sysvsmRs[sysname]
            g_sysvsmR.SetPoint(g_sysvsmR.GetN(),mR,thesys)


    # plot
    graphs=sorted(g_sysvsmRs.values(),key=lambda x: x.GetTitle(),reverse=True)
    plottools.graphs(graphs,opt='APL',xtitle='m_{Z\'} [GeV]',ytitle='(N_{m_{Z\'}#pm50,var}-N_{m_{Z\'}#pm50,nom})/N_{m_{Z\'}#pm50,nom}',legend=(0.2,0.9),yrange=(0,0.15),**kwargs)
    canvastools.save('allsys.pdf')
            

        

def main(paths,**kwargs):
    canvastools.savesetup('accuncert',**kwargs)

    selectiontools.loop(plotAccUncert,*paths,**kwargs)

    canvastools.savereset()
