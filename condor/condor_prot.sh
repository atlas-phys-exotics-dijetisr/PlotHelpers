#!/bin/bash
uname -a

CMDFILE=${1}
let CMDIDX=${2}+1
CMD=$(sed "${CMDIDX}q;d" ${CMDFILE})

# Setup workdir
export HOME=$(pwd)


# setup code
mkdir code
cd code
tar -xjf ../code.tar.bz2
tar -xjf ../data.tar.bz2

# ATLAS
echo "Setting up ATLAS ..."
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=${HOME}/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet


# ROOT
if [ -e RootCore.par ]; then
    echo "Setting up RootCore ..."
    tar -xzf RootCore.par
    source RootCore/RootCoreBin/local_setup.sh
    lsetup fax
else
    echo "Setting up Root ..."
    lsetup fax root
fi

export XrdSecGSISRVNAMES="*"
export X509_USER_PROXY=${HOME}/gridproxy.cert

source setup.sh

echo "${CMD[@]}"
./prot.py "${CMD[@]}"

for out in ${@:3}
do
    if [ -e ${out} ]; then
	mv ${out} ${HOME}/${out}.${2}
    fi
done
