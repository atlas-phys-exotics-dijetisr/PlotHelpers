#!/bin/bash

if [ ${#} != 1 ]; then
    echo "usage: ${0} payload.tar.bz2"
    exit -1
fi

DIRNAME=${1}

cd "$( dirname "${BASH_SOURCE[0]}" )"

# Prepare submit directory
cp prot_merge.sh ${DIRNAME}
cp condor_prot.sh ${DIRNAME}
cp condor_prot.submit ${DIRNAME}
cd ${DIRNAME}
mkdir logs

# Submit
condor_submit_dag condor_prot.dag
condor_wait -echo condor_prot.dag.dagman.log 
