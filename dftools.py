import glob

import ROOT

from prot import plottools

def normalization(filelist,weighted=True):
    n=0.
    for path in filelist:
        fh=ROOT.TFile.Open(path)
        h=fh.Get('MetaData_EventCount')
        n+=h.GetBinContent(3 if weighted else 1)
        fh.Close()
        
    return n

def makedf(globpath,lumi=1,weighted=True):
    n=normalization(glob.glob(globpath),weighted=weighted)

    df=ROOT.RDataFrame('outTree',globpath)
    return df.Define('w','{lumi}*weight/{n}'.format(lumi=lumi,n=n))

def makedf_filelist(filelist,lumi=1,weighted=True):
    # Read filelist
    fh=open(filelist)
    fl=[]
    for line in fh:
        line=line.strip()
        if line=='' or line.startswith('#'): continue
        fl.append(line)
    fh.close()

    # Determine normalization
    n=normalization(fl,weighted=weighted)

    # Make std::vector list of file with a tree
    stdfl=ROOT.std.vector(ROOT.string)()
    for path in fl:
        fh=ROOT.TFile.Open(path)
        if fh.Get('outTree')!=None:
            stdfl.push_back(path)
        fh.Close()

    if stdfl.size()==0: return None # Empty!
        
    # Make!
    df=ROOT.RDataFrame('outTree',stdfl)
    return df.Define('w','{lumi}*weight/{n}'.format(lumi=lumi,n=n))

def makedf_dir(dirpath,dirfilter='*',lumi=1,weighted=True):
    globpath=dirpath+'/'+dirfilter
    fl=glob.glob(globpath)
    
    n=normalization(fl,weighted=weighted)

    stdfl=ROOT.std.vector(ROOT.string)()
    stdfl+=fl
    df=ROOT.RDataFrame('outTree',stdfl)
    return df.Define('w','{lumi}*weight/{n}'.format(lumi=lumi,n=n))

def PtEtaPhiE(df,particle):
    codeP4='ROOT::VecOps::RVec<TLorentzVector> {p}_p4(n{p}); for(uint i=0;i<n{p};i++) {{ {p}_p4[i].SetPtEtaPhiE({p}_pt[i],{p}_eta[i],{p}_phi[i],{p}_E[i]); }} return {p}_p4;'
    return df.Define('{}_p4'.format(particle), codeP4.format(p=particle))

def PtEtaPhiM(df,particle):
    codeP4='ROOT::VecOps::RVec<TLorentzVector> {p}_p4(n{p}); for(uint i=0;i<n{p};i++) {{ {p}_p4[i].SetPtEtaPhiM({p}_pt[i],{p}_eta[i],{p}_phi[i],{p}_m[i]); }} return {p}_p4;'
    return df.Define('{}_p4'.format(particle), codeP4.format(p=particle))
